

//-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.

var invalidData=0;
 
var data2=0;
$('#checkBtn1').on('click', function(){
	$('#legend').empty();
 	  $('#legend').append('<div id="square"></div><p id="invalidRecord">Invalid Records: </p><div id="square2"></div><p id ="validRecord">ValidRecords: ');
 	 
			test();
			test1();
			var valid = data2-invalidData;
		    var PieData = [
		      {
		        value: invalidData,
		        color: "#f56954",
		        highlight: "#f56954",
		        label: "Invalid Data"
		      },
		      {
		        value: valid,
		        color: "#00a65a",
		        highlight: "#00a65a",
		        label: "Valid Data"
		      }
		    ];
		    pieChart.Doughnut(PieData, pieOptions);
		    timeForUpload();			
		  
		    $('#invalidRecord').append(invalidData);
		    $('#validRecord').append(valid);
		});
		
var timeForUpload = function(){
	console.log("UPLOAD");
	$.ajax({
		type : 'GET',
		url : '/PiedPiperProject/rest/import/timeForUpload',
		dataType : "json",
		success : function(data) {
			$('#alertTime').empty();
			$('#alertTime').append(
					'<p> File Uploaded in ' + data + ' seconds.')
			$('#alertTime').show();
		}

	});

}

		var validCount = function(){
			$.ajax({
				type : 'GET',
				url : "http://localhost:8090/PiedPiperProject/rest/Data/totalCount",
				dataType : 'TEXT',
				success : function(data){
					alert(data);
					
					
				}
			});
		}

		var invalidCount = function(){
			$.ajax({
				type : 'GET',
				url : "http://localhost:8090/PiedPiperProject/rest/Data/invalidCount",
				dataType : 'TEXT_PLAIN',
				success : function(data){
					
					console.log(data);
					alert(data);
					}
			});
		}
		
		function test() {
		    myFunction(function(d) {
		        
		        console.log(d);
		        data2 =d;
		        console.log("data2 " +data2);
		    });
		}

		function myFunction(callback) {
		    
		    $.ajax({
		        url: 'http://localhost:8090/PiedPiperProject/rest/admin/totalCount',
		        data: 'text',
		        success: function (resp) {
		            data2 = parseInt(resp);
		            callback(data2);
		          //  console.log(data2);
		        },
		        error: function () {}
		    }); // ajax asynchronus request 
		    //the following line wouldn't work, since the function returns immediately
		    //return data; // return data from the ajax request
		}

		
		function test1() {
		    myFunction1(function(d) {
		        //processing the data
		        console.log(d);
		        invalidData=d;
		        console.log("data1 " +invalidData);
		    });
		}

		function myFunction1(callback) {
		    
		    $.ajax({
		        url: 'http://localhost:8090/PiedPiperProject/rest/admin/invalidCount',
		        data: 'text',
		        success: function (resp) {
		            invalidData = parseInt(resp);
		            callback(totalData);
		            //console.log(data1);
		        },
		        error: function () {}
		    }); // ajax asynchronus request 
		    //the following line wouldn't work, since the function returns immediately
		    //return data; // return data from the ajax request
		}

		
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    
   
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
