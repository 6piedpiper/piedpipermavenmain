//All JavaScript for Network Management Engineers

var getDataURL = new URL("http://localhost:8090/PiedPiperProject/rest/neng_data/");

$(document).on("click", "#formBtnNeng", function() {
	var startDate = $('#startDateInput').val();
	var startTime = $('#startTimeInput').val();
	var endDate = $('#endDateInput').val();
	var endTime = $('#endTimeInput').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				getCountImsiAndDuration(startDateTimeString, endDateTimeString);
			}else{
				$('#NE_imsiTable').DataTable().clear();
				var element = document.getElementById("validationAlert");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime').show();
                $('#invalidTime').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountImsiAndDuration(startDateTimeString, endDateTimeString);
		}
	}else{
		$('#NE_imsiTable').DataTable().clear();
		var element = document.getElementById("validationAlert");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate').show();
		$('#invalidDate').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate').alert('hide');
         }); 
	}

});

$(document).on("click", "#formBtnNeng2", function() {
	var startDate = $('#startDateInput1').val();
	var startTime = $('#startTimeInput1').val();
	var endDate = $('#endDateInput1').val();
	var endTime = $('#endTimeInput1').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				getCountTop10Imsi(startDateTimeString, endDateTimeString);
			}else{
				$('#NE_imsiTop10Table').DataTable().clear();
				var element = document.getElementById("validationAler1t");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime1">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime1').show();
                $('#invalidTime1').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime1').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountTop10Imsi(startDateTimeString, endDateTimeString);
		}
	}else{
		$('#NE_imsiTop10Table').DataTable().clear();
		var element = document.getElementById("validationAlert1");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate1">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate1').show();
		$('#invalidDate1').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate1').alert('hide');
         }); 
	}
});

$(document).on("click", "#formBtnMarket", function() {
	var startDate = $('#startDateInput2').val();
	var startTime = $('#startTimeInput2').val();
	var endDate = $('#endDateInput2').val();
	var endTime = $('#endTimeInput2').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				getCountMarketandOperator(startDateTimeString, endDateTimeString);
			}else{
				$('#NE_marketTable').DataTable().clear();
				var element = document.getElementById("validationAlert2");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime2">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime2').show();
                $('#invalidTime2').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime2').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountMarketandOperator(startDateTimeString, endDateTimeString);
		}
	}else{
		$('#NE_marketTable').DataTable().clear();
		var element = document.getElementById("validationAlert2");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate2">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate2').show();
		$('#invalidDate2').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate2').alert('hide');
         }); 
	}
	
});

//clicked Query Variables
var count1 = 2;
var count2 = 2;
var count3 = 2;
var count4 = 2;
var count5 = 2;
var count6 = 2;
var count7 = 2;
var count8 = 2;
var count9 = 2;
var count10 = 2;
var openCloseBox = function() {

	// Click on a side nav link Open the Query Box:
	$(document).on('click', '#left a', function() {
		
		console.log('...clicked on left nav -> id: ' + this.id);
		
		if (this.id == 'linkQuery1') {
			if (count1 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse in');
				$('#queryBox1a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count1++;
				console.log('count1:: ' + count1);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse');
				$('#queryBox1a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count1++;
				console.log('count1:: ' + count1);
			}
		}
		
		if (this.id == 'linkQuery2') {
			if (count2 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse in');
				$('#queryBox2a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count2++;
				console.log('count2:: ' + count2);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse');
				$('#queryBox2a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count2++;
				console.log('count2:: ' + count2);
			}
		}
		
		if (this.id == 'linkQuery3') {
			if (count3 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count3:: ' + count3);
				$('#queryBox3').attr('class', 'body collapse in');
				$('#queryBox3a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count3++;
				console.log('count3:: ' + count3);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count3:: ' + count3);
				$('#queryBox3').attr('class', 'body collapse');
				$('#queryBox3a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count3++;
				console.log('count3:: ' + count3);
			}
		}
		
		if (this.id == 'linkQuery4') {
			if (count4 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count4:: ' + count4);
				$('#queryBox4').attr('class', 'body collapse in');
				$('#queryBox4a').attr('class', 'body collapse in');
				$('#queryBox4b').attr('class', 'body collapse in');
				$('#queryBox4c').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count4++;
				console.log('count4:: ' + count4);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count4:: ' + count4);
				$('#queryBox4').attr('class', 'body collapse');
				$('#queryBox4a').attr('class', 'body collapse');
				$('#queryBox4b').attr('class', 'body collapse');
				$('#queryBox4c').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count4++;
				console.log('count4:: ' + count4);
			}
		}
		
		if (this.id == 'linkQuery5') {
			if (count5 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count5:: ' + count5);
				$('#queryBox5').attr('class', 'body collapse in');
				$('#queryBox5a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count5++;
				console.log('count5:: ' + count5);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count5:: ' + count5);
				$('#queryBox5').attr('class', 'body collapse');
				$('#queryBox5a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count5++;
				console.log('count5:: ' + count5);
			}
		}
		if (this.id == 'linkQuery6') {
			if (count6 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count6:: ' + count6);
				$('#queryBox6').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count6++;
				console.log('count6:: ' + count6);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count6:: ' + count6);
				$('#queryBox6').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count6++;
				console.log('count6:: ' + count6);
			}
		}
		if (this.id == 'linkQuery7') {
			if (count7 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count7:: ' + count7);
				$('#queryBox7').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count7++;
				console.log('count7:: ' + count7);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count7:: ' + count7);
				$('#queryBox7').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count7++;
				console.log('count7:: ' + count7);
			}
		}
		
		if (this.id == 'linkQuery8') {
			if (count8 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count8:: ' + count8);
				$('#queryBox8').attr('class', 'body collapse in');
				$('#queryBox8a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count8++;
				console.log('count8:: ' + count8);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count8:: ' + count8);
				$('#queryBox8').attr('class', 'body collapse');
				$('#queryBox8a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count8++;
				console.log('count8:: ' + count8);
			}
		}
		
		if (this.id == 'linkQuery9') {
			if (count9 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count9:: ' + count9);
				$('#queryBox9').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count9++;
				console.log('count9:: ' + count9);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count9:: ' + count9);
				$('#queryBox9').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count9++;
				console.log('count9:: ' + count9);
			}
		}
		
		if (this.id == 'linkQuery10') {
			if (count10 % 2 === 0) {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "#449D44");
				console.log('count10:: ' + count10);
				$('#queryBox10').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count10++;
				console.log('count10:: ' + count10);
			} else {
				// Change the Color of the Selected Query in side Nav bar
				$(this).css("background-color", "");
				console.log('count10:: ' + count10);
				$('#queryBox10').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count10++;
				console.log('count10:: ' + count10);
			}
		}

	});

};

$(document).ready(function(){
	findAllUniquePhoneModel();
	findAllUniqueImsi();
	// Set all the Query Boxes to Close:
	$('#queryBox1').attr('class', 'body collapse');
	$('#queryBox1a').attr('class', 'body collapse');
	$('#queryBox2').attr('class', 'body collapse');
	$('#queryBox2a').attr('class', 'body collapse');
	$('#queryBox3').attr('class', 'body collapse');
	$('#queryBox3a').attr('class', 'body collapse');
	$('#queryBox4').attr('class', 'body collapse');
	$('#queryBox4a').attr('class', 'body collapse');
	$('#queryBox4b').attr('class', 'body collapse');
	$('#queryBox4c').attr('class', 'body collapse');
	$('#queryBox5').attr('class', 'body collapse');
	$('#queryBox5a').attr('class', 'body collapse');
	$('#queryBox6').attr('class', 'body collapse');
	$('#queryBox7').attr('class', 'body collapse');
	$('#queryBox8').attr('class', 'body collapse');
	$('#queryBox8a').attr('class', 'body collapse');
	$('#queryBox9').attr('class', 'body collapse');
	$('#queryBox10').attr('class', 'body collapse');
	$('.fa-minus').attr('class', 'fa fa-plus');

	openCloseBox();
	
});

var findAllUniqueImsi = function(){
	$.get({
		url : "rest/csr_data/" + 'distinctImsi',
		dataType : "json",
		success : function(data){
			$.each(data,function(index,basedata){
				$('#imsiSelectForCauseCodes').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
			});
		}
	})
}

$(document).on("click", "#imsiCauseCodeBtn", function() {
	$('#getCauseCodes1').DataTable().destroy();
	var imsi = $('#imsiSelectForCauseCodes option:selected').text();
	findCauseCodesByIMSI1(imsi);
});

var findCauseCodesByIMSI1 = function(IMSI){
	console.log(IMSI);
	$('#getCauseCodes1').DataTable().clear();
	$.ajax({
		type : 'GET',
		url : 'rest/csr_data/' +'getUniqueCauseCode/'+IMSI,
		dataType : "json",
		success : function(data) {
			$.each(data, function(index, object) {
				$('#getCauseCodes1').DataTable().row.add([object[0],object[1]]);
			});
			$('#getCauseCodes1').DataTable().draw();
		}
	});
}
	

var findAllUniquePhoneModel = function(){
	$.get({
		url : getDataURL + 'getPhoneModels',
		dataType : "json",
		success : populatePhoneDropdown
	});
}

$(document).on("click", "#btnPhoneModelSubmit", function() {
	$('#NE_phoneModelCauseCode').DataTable().destroy();
	var tacNo = $('#selectPhoneModel option:selected').val();
	findByPhoneModel(tacNo);
});

var findByPhoneModel = function(tac) {
	$.ajax({
		type : 'GET',
		url : getDataURL + 'model/'+ tac,
		dataType : "json",
		success : function(data) {
			$('#NE_phoneModelCauseCode').DataTable().clear();
			 $.each(data, function(index, object) {
				 $('#NE_phoneModelCauseCode').DataTable().row.add([object[0], object[1], object[2],object[3]]);
			 });
			 $('#NE_phoneModelCauseCode').DataTable().draw();	
			 percentageChart1();
		 }
	 });
}

var populatePhoneDropdown = function(data) {
	$.each(data,function(index,userentity){
		$('#selectPhoneModel').append('<option value="'+userentity.tac+'" selected="selected">'+userentity.model+'('+userentity.manufacturer+')</option>');
	});
}



var getCountMarketandOperator = function(startDate, endDate) {
	 $.get({
		 url : getDataURL + "countMarketOpCell/"+ startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#NE_marketTable').DataTable().clear();
			 console.log("cleared")	
			 console.log(data);
			 var countTotal = 0;
			 console.log("Leann" + countTotal);
			 $.each(data, function(index, object) {
				 $('#NE_marketTable').DataTable().row.add([object[0], object[1], object[2],object[3],object[4],object[5]]);
				countTotal = countTotal + object[0];
				
				
			 });
			 
			 $('#NE_marketTable').DataTable().draw();
			 percentageChart(countTotal);
			 console.log("drawn");
		 }
	 });
}

var phoneArray;
var phoneModel;
var averageFailureTime;

var getCountImsiAndDuration = function(startDate, endDate) {
	$.get({
		 url : getDataURL + "getDistinctImsi/"+ startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#NE_imsiTable').DataTable().clear();
			 console.log("cleared");
			 phoneArray = data;
			 console.log(new Date())	;
			 $.each(data, function(index, object) {
				 $('#NE_imsiTable').DataTable().row.add([object[0], object[1], object[2]]);
			 });
			 $('#NE_imsiTable').DataTable().draw();
			 $('#NE_imsiTable tbody').on('click', 'tr', function () {
			        var data1 = $('#NE_imsiTable').DataTable().row(this).data();
			        for(var i=0;i<phoneArray.length;i++){
			        	if(data1[0]===phoneArray[i][0]){
			        		phoneModel = phoneArray[i][3];
			        		averageFailureTime = data1[2]/data1[1];
			        		break;
			        	}
			        }
			 });
			 console.log(new Date());
			 Chart1(phoneArray,endDate);
		}
	 });
}

var getCountTop10Imsi = function(startDate, endDate) {
	$.get({
		 url : getDataURL + "getTop10ImsiFailures/"+ startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#NE_imsiTop10Table').DataTable().clear();
			 $.each(data, function(index, object) {
				 $('#NE_imsiTop10Table').DataTable().row.add([object[0], object[1]]);
			 });
			 $('#NE_imsiTop10Table').DataTable().draw();	
			 drawBestChart(startDate,endDate);
		 }
	 });
}

var validateTwoDates = function(startDate,endDate){
	if(startDate>endDate){
		return false
	}else{
		return true;
	}
}

var validateTwoTimes = function(startTime,endTime){
	if(startTime>endTime){
		return false
	}else{
		return true;
	}
}