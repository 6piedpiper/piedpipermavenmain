var Chart1 = function(phoneData, endDate) {
	google.charts.setOnLoadCallback(drawIMSIMainChart);
	function drawIMSIMainChart() {

		var dataArray = new Array();
		dataArray.push(new Array("IMSI", "IMSI, Average Duration"));
		for (var i = 0; i < phoneData.length; i++) {
			var imsi = (parseInt(phoneData[i][0]));
			var average = (parseInt(phoneData[i][2]) / parseInt(phoneData[i][1]));
			console.log(imsi,average);
			dataArray.push(new Array(imsi, average));
		}
		var data = google.visualization.arrayToDataTable(dataArray);

		var options = {
			title : 'Average Duration per Failure for IMSI',
			vAxis : {
				title : 'Average Duration (ms)'
			},
			hAxis : {
				title : 'IMSI'
			},
			explorer : {
				axis : 'horizontal',
				keepInBounds : true,
				maxZoomIn : 0.01
			},

			pointSize : 2,

		};
		var chart = new google.visualization.ScatterChart(document
				.getElementById('imsi_chart_div'));
		chart.draw(data, options);

		google.visualization.events.addListener(chart, 'select', function() {
			var imsi = dataArray[(chart.getSelection()[0].row) + 1][0];
			mainBarChartSelect(imsi);
		});

	}

	var imsiArray;
	function mainBarChartSelect(IMSI) {
		var resultArray = new Array();
		resultArray.push(new Array("DateTime", "Duration"));
		$
				.ajax({
					type : 'GET',
					url : "http://localhost:8090/PiedPiperProject/rest/neng_data/getAllWithIMSI/"
							+ IMSI,
					dataType : "json",
					success : function(data) {
						imsiArray = data;
						$.each(data, function(index, object) {
							var duration = object.duration;
							var date = object.failureDate;
							if (date < endDate) {
								resultArray.push(new Array(date, duration));
							}
						});
						drawIMSIChildChart(resultArray, IMSI);
					}
				});

	}

	function drawIMSIChildChart(resultArray, IMSI) {
		var data = google.visualization.arrayToDataTable(resultArray);
		var options = {
			title : 'Date/Time Chart for IMSI: ' + IMSI,
			vAxis : {
				title : 'Duration (ms)'
			},
			hAxis : {
				title : 'Date/Time'
			},
			seriesType : 'bars',

		};
		var childChart = new google.visualization.ComboChart(document
				.getElementById('imsi_chart_div'));
		childChart.draw(data, options);
		google.visualization.events
				.addListener(
						childChart,
						'select',
						function() {
							var date = resultArray[(childChart.getSelection()[0].row) + 1][0];
							var duration = resultArray[(childChart
									.getSelection()[0].row) + 1][1];
							childChart.setSelection();
							childChartSelect(date, duration)
						});
	}

	function childChartSelect(date, duration) {
		for (var i = 0; i < imsiArray.length; i++) {
			if (date === imsiArray[i].failureDate) {
				if (duration === imsiArray[i].duration) {
					var failureData = imsiArray[i];
				}
			}
		}
		drawIMSIFailureDataTable(failureData);
	}

	function drawIMSIFailureDataTable(failureData) {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date/Time')
		data.addColumn('number', 'Event ID');
		data.addColumn('number', 'Failure Class');
		data.addColumn('number', 'UEType');
		data.addColumn('number', 'Market');
		data.addColumn('number', 'Operator');
		data.addColumn('number', 'Cell ID');
		data.addColumn('number', 'Duration');
		data.addColumn('number', 'Cause Code');
		data.addColumn('string', 'NE Version');
		data.addColumn('number', 'IMSI');
		data.addColumn('number', 'Hier3_ID');
		data.addColumn('number', 'Hier32_ID');
		data.addColumn('number', 'Hier321_ID');
		data.addRows(1);
		data.setCell(0, 0, failureData.failureDate);
		data.setCell(0, 1, failureData.eventId);
		data.setCell(0, 2, failureData.failureClass);
		data.setCell(0, 3, failureData.uetype);
		data.setCell(0, 4, failureData.marketNo);
		data.setCell(0, 5, failureData.operatorNo);
		data.setCell(0, 6, failureData.cellId);
		data.setCell(0, 7, failureData.duration);
		data.setCell(0, 8, failureData.causeCode);
		data.setCell(0, 9, failureData.nEVersion);
		data.setCell(0, 10, failureData.imso);
		data.setCell(0, 11, failureData.hier3_ID);
		data.setCell(0, 12, failureData.hier32_ID);
		data.setCell(0, 13, failureData.hier321_ID);

		var options = {
			allowHtml : true
		};

		var table = new google.visualization.Table(document
				.getElementById('table_div'));
		table.draw(data, options);

		google.visualization.events.addListener(table, 'select', function() {
			$('#table_div').empty();
			drawIMSIMainChart();

		});
	}

};