//All JavaScript for Customer Service Rep queries

var rootUrl = "/PiedPiperProject/rest/csr_data/";


$(document).on("click", "#imsiBtn", function() {
	$('#myTable').DataTable().destroy();
	var imsi = $('#imsiSelect option:selected').text();
	console.log(imsi);
	findByIMSI(imsi);
});

$(document).on("click", "#imsiBtn2", function() {
	$('#myTable2').DataTable().destroy();
	var imsi = $('#imsiSelect2 option:selected').text();
	console.log(imsi);
	var startDate = $('#startDateInput').val();
	var startTime = $('#startTimeInput').val();
	var endDate = $('#endDateInput').val();
	var endTime = $('#endTimeInput').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				console.log(startDateTimeString+"&"+endDateTimeString);
				getCountImsi(imsi, startDateTimeString, endDateTimeString);
			}else{
				$('#myTable2').DataTable().clear();
				$('#count').text("0");
				var element = document.getElementById("validationAlert");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime').show();
                $('#invalidTime').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountImsi(imsi, startDateTimeString, endDateTimeString);
		}
	}else{
		$('#myTable2').DataTable().clear();
		$('#count').text("0");
		var element = document.getElementById("validationAlert");
			element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate">'+
		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
		    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
			$('#invalidDate').show();
			$('#invalidDate').fadeTo(4000, 500).slideUp(500, function(){
				$('#invalidDate').alert('hide');
             }); 
	}
	
});

$(document).on("click", "#imsiCauseCodeBtn", function() {
	$('#getCauseCodes').DataTable().destroy();
	var imsi = $('#imsiSelectForCauseCodes option:selected').text();
	findCauseCodesByIMSI(imsi);
});

var findAllUniqueImsi = function(){
	$.get({
		url : rootUrl + 'distinctImsi',
		dataType : "json",
		success : populateDropdown
	});
}

var findCauseCodesByIMSI = function(IMSI){
	console.log(IMSI);
	$.ajax({
		type : 'GET',
		url : rootUrl +'getUniqueCauseCode/'+IMSI,
		dataType : "json",
		success : function(data) {
			$('#getCauseCodes').DataTable().clear();
			$.each(data, function(index, object) {
				console.log(object);
				$('#getCauseCodes').DataTable().row.add([object[0],object[1]]);
			});
			$('#getCauseCodes').DataTable().draw();
		}
	});
}

var findByIMSI = function(IMSI) {
	if(IMSI !== ""){
	$.ajax({
		type : 'GET',
		url : rootUrl + IMSI,
		dataType : "json",
		success : function(data) {
			$('#myTable').DataTable().clear();
			$.each(data, function(index, object) {
				 $('#myTable').DataTable().row.add([object[0], object[1], object[2],object[3]]);
			 });
			$('#imsiInput').val("");
		}
	});
}
	else{
		alert("No IMSI entered");
	}
}


var getCountImsi = function(imsi, startDate, endDate) {
	 $.get({
		 url : rootUrl + "getCountForIMSI/"+ imsi+"&"+startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#myTable2').DataTable().clear();
			 console.log("cleared")	
			 console.log(data);
			 console.log(imsi+', '+data);
			 
			 // Add to our Count Badge
			 $('#count').text(data);
			 $('#IMSI').text(imsi);
			 
			 $('#myTable2').DataTable().row.add([imsi, data]);
			 $('#myTable2').DataTable().draw();	
			 console.log("drawn");
		 }
	 });
}


var validateTwoDates = function(startDate,endDate){
	if(startDate>endDate){
		return false
	}else{
		return true;
	}
}

var validateTwoTimes = function(startTime,endTime){
	if(startTime>endTime){
		return false
	}else{
		return true;
	}
}

var populateDropdown = function(data){
	$.each(data,function(index,basedata){
		$('#imsiSelect').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		$('#imsiSelect2').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		$('#imsiSelectForCauseCodes').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
	});
}
//clicked Query Variables
var count1 = 2;
var count2 = 2;
var count3 = 2;
var count4 = 2;
var openCloseBox = function() {

	// Click on a side nav link Open the Query Box:
	$(document).on('click', '#left a', function() {

		console.log('...clicked on left nav -> id: ' + this.id);
		
		if (this.id == 'linkQuery1') {
			if (count1 % 2 === 0) {
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse in');
				$('#queryBox1a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count1++;
				console.log('count1:: ' + count1);
			} else {
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse');
				$('#queryBox1a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count1++;
				console.log('count1:: ' + count1);
			}
		}
		
		if (this.id == 'linkQuery2') {
			if (count2 % 2 === 0) {
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count2++;
				console.log('count2:: ' + count2);
			} else {
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count2++;
				console.log('count2:: ' + count2);
			}
		}
		
		if (this.id == 'linkQuery3') {
			if (count3 % 2 === 0) {
				console.log('count3: ' + count3);
				$('#queryBox3').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count3++;
				console.log('count3:: ' + count3);
			} else {
				console.log('count3:: ' + count3);
				$('#queryBox3').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count3++;
				console.log('count3:: ' + count3);
			}
		}
		
		if (this.id == 'linkQuery4') {
			if (count4 % 2 === 0) {
				console.log('count4: ' + count4);
				$('#queryBox4').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count4++;
				console.log('count4:: ' + count4);
			} else {
				console.log('count4:: ' + count4);
				$('#queryBox4').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count4++;
				console.log('count4:: ' + count4);
			}
		}

	});

};

$(document).ready(function(){
	findAllUniqueImsi();
	
	// Set all the Query Boxes to Close:
	$('#queryBox1').attr('class', 'body collapse');
	$('#queryBox1a').attr('class', 'body collapse');
	$('#queryBox2').attr('class', 'body collapse');
	$('#queryBox3').attr('class', 'body collapse');
	$('#queryBox4').attr('class', 'body collapse');
	$('.fa-minus').attr('class', 'fa fa-plus');

	openCloseBox();
	
});