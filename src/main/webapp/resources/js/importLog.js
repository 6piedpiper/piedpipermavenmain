/**
 * 
 */
var logtable = $('#logTable').DataTable();
var getImportLog = function(){
	$.get({
		url : "http://localhost:8090/PiedPiperProject/rest/import/importLog",
		dataType :"json",
		success : renderLog
	});
}

var renderLog = function(data){
	logtable.destroy();
	logtable = $('#logTable').DataTable({
		data :data,
		columns : [ {
			'data': 'dateImported'
		},{ 'data': 'filename' 
		},{ 'data': 'successfulImports'
		},{ 'data': 'failedImports'
		},{ 'data': 'duration'
		}]
	});
	console.log('table complete');
}

$(document).ready(function(){
	getImportLog();

	var	myInterval = setInterval(function() {
		getImportLog();
	}, 3000); // poll every 1.5 seconds

	document.addEventListener('visibilitychange', function() {
		//when the page becomes hidden then stop polling
		if (document.hidden) {
			clearInterval(myInterval);
		} else {
			myInterval = setInterval(function() {
				getImportLog();
			}, 3000); // poll every 1.5 seconds

		}
	});
});