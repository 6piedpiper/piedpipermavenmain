//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
google.charts.load('current', {
		'packages' : [ 'corechart','table' ]
	});

var percentageChart = function(countTotal) {
	
	
	var table = $('#NE_marketTable').DataTable();

	var array = new Array();
	var cellArray = new Array();
	var arrayName = new Array();
	var arrayCountry = new Array();
	var arrayCount = new Array();
	var UniqueOperators = new Array();
	var UniqueCountry = new Array();
	var UniqueOperatorCountFailure = new Array();
	var runningCount;
	var arrayNameWithCount = new Array();
	$('#NE_marketTable tbody tr').each(function() {
		var name = $(this).find("td:last-child").html();
		var cell = $(this).find("td:first").html();
		var country = $(this).find("td:nth-last-child(2)").html();
		var cellID = $(this).find("td:nth-last-child(3)").html();
		arrayCount.push(cell);
		// cell = (cell/countTotal)*100;
		cell = Math.round(cell);
		arrayName.push(name);
		array.push(cell);
		cellArray.push(cellID);
		arrayCountry.push(country);

	});

	$.each(arrayName, function(i, el) {
		if ($.inArray(el, UniqueOperators) === -1)
			UniqueOperators.push(el);

	});

	var UniqueOperatorCount = new Array();
	for (var i = 0; i < UniqueOperators.length; i++) {
		UniqueOperatorCount[i] = 0;
	}

	for (var i = 0; i < UniqueOperators.length; i++) {

		$('#NE_marketTable tbody tr').each(
				function() {
					var name2 = $(this).find("td:last-child").html();
					cell2 = parseInt(UniqueOperatorCount[i]);

					if (UniqueOperators[i] === name2) {

						var temp = $(this).find("td:first").html();

						UniqueOperatorCount[i] = UniqueOperatorCount[i]
								+ parseInt(temp);
					}

					
				});

	}
	
	
	
	$.each(arrayCountry, function(i, el) {
		if ($.inArray(el, UniqueCountry) === -1)
			UniqueCountry.push(el);

	});

	var UniqueCountryCount = new Array();
	for (var i = 0; i < UniqueCountry.length; i++) {
		UniqueCountryCount[i] = 0;
	}

	for (var i = 0; i < UniqueCountry.length; i++) {

		$('#NE_marketTable tbody tr').each(
				function() {
					var country2 = $(this).find("td:nth-last-child(2)").html();
					cell2 = parseInt(UniqueCountryCount[i]);

					if (UniqueCountry[i] === country2) {

						var temp = $(this).find("td:first").html();

						UniqueCountryCount[i] = 	UniqueCountryCount[i]
								+ parseInt(temp);
					}

					
				});

	}

	google.charts.setOnLoadCallback(drawChart);
	function drawChart() {
		var data = new google.visualization.DataTable();
	    data.addColumn('string', 'Operators');
	    data.addColumn('number', 'Count');
			
			for(var i = 0; i < UniqueOperators.length; i++){
				
				 data.addRows([
				               [UniqueOperators[i], parseInt(UniqueOperatorCount[i])]
				             ]);
				 }
			 
		var options = {

			pieSliceText : 'label',
			title : 'Percentage of Call Failures Per Operator',
			pieStartAngle : 100,
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('pieChart'));
		chart.draw(data, options);
	}

	Array.prototype.unique = function() {
		var arr = this;
		return $.grep(arr, function(v, i) {
			return $.inArray(v, arr) === i;
		});
	}

	Chart.defaults.global.responsive = true;

	
	google.charts.setOnLoadCallback(drawVisualization);
	var average = countTotal / 10;
	
	function drawVisualization() {

		var data = google.visualization.arrayToDataTable([
				[ 'Operators', arrayName[0]+ "Cell ID :" +cellArray[0], arrayName[1]+ "Cell ID :" +cellArray[1], arrayName[2]+ "Cell ID :" +cellArray[2],
						arrayName[3]+ "Cell ID :" +cellArray[3], arrayName[4]+ "Cell ID :" +cellArray[4], arrayName[5]+ "Cell ID :" +cellArray[5], arrayName[6]+ "Cell ID :" +cellArray[6],
						arrayName[7]+ "Cell ID :" +cellArray[7], arrayName[8]+ "Cell ID :" +cellArray[8], arrayName[9]+ "Cell ID :" +cellArray[0]],
				[ '', parseInt(arrayCount[0]), parseInt(arrayCount[1]),
						parseInt(arrayCount[2]), parseInt(arrayCount[3]),
						parseInt(arrayCount[4]), parseInt(arrayCount[5]),
						parseInt(arrayCount[6]), parseInt(arrayCount[7]),
						parseInt(arrayCount[8]), parseInt(arrayCount[9]) ]
	
		]);
		
		

		var options = {
			title : 'Top 10 Market/Operator/Cell Id Call Failures',
			vAxis : {
				title : 'Count'
			},
			hAxis : {
				title : 'Operators'
			},
			seriesType : 'bars',
			legend: {position: 'none'},
			
		};

		var chart = new google.visualization.ComboChart(document
				.getElementById('chart_div'));
		chart.draw(data, options);
	}

	
	

	
	google.charts.setOnLoadCallback(drawRegionsMap);

	function drawRegionsMap() {
		
		var data1 =  new google.visualization.DataTable();
		data1.addColumn('string', 'Country');
		data1.addColumn('number', 'Count');
			
		for(var i = 0; i < UniqueCountry.length; i++){
			if(UniqueCountry[i]=== "Guadeloupe-France"){
				$('#legend').empty();
			 	  $('#legend').append('<div id="square"></div><p id="invalidRecord">Guadeloupe-France Failure Count: '+UniqueCountryCount[i]+' </p>');
			}
			// United States of America
			if(UniqueCountry[i]=== "United States of America"){
				UniqueCountry[i] ="United States";
			}
			//Australia
			if(UniqueCountry[i]=== "Australia"){
				UniqueCountry[i] ="Australia";
			}
			if(UniqueCountry[i]==="Antigua and Barbuda"){
				$('#legend').empty();
			 	  $('#legend').append('<div id="square"></div><p id="antiguaBarbuda">Antigua and Barbuda Failure Count: '+UniqueCountryCount[i]+' </p>');
			}
		}
		
		for(var i = 0; i < UniqueCountry.length; i++){
				
				 data1.addRows([
				               [UniqueCountry[i], parseInt(UniqueCountryCount[i])]
			             ]);
				 }
 var options={
		 colorAxis: {colors: ['yellow', 'orange', '#e31b23']},
 }

		var chart1 = new google.visualization.GeoChart(document
				.getElementById('regions_div'));

		chart1.draw(data1, options);
	}

};
