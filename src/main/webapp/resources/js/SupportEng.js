//All JavaScript for Support Engineer queries

	
var rootUrl = new URL("http://localhost:8090/PiedPiperProject/rest/seng_data/");

var getListFailuresBetweenTimes = function(startDate, endDate) {
	$('#SEng_table01').DataTable().destroy();
	$.get({
		url : rootUrl+'getListBetweenTimes/' + startDate + "&" + endDate,
		dataType : "json",
		success : function(data) {
			console.log("inside get")
			$('#SEng_table01').DataTable({
				data : data,
				paging : true,
				searching : true,
				sort : true,
				columns : [ {
					'data' : 'failureDate'
				}, {
					'data' : 'imso'
				} ],
			});
		}

	});
}

var getPhoneModels = function(){
	$.get({
		url : rootUrl + "getPhoneModels",
		dataType : "json",
		success : function(data){
			$.each(data,function(index,uetype){
		$('#phoneModelSelect').append('<option value="'+uetype.tac+'" selected="selected">'+uetype.model+'('+uetype.manufacturer+')</option>');			});
		}
	});
}

var getFailureCountForModel = function(startDate, endDate, tacNo){
	$.get({
		url : rootUrl + "getCount/"+tacNo+"&"+startDate+"&"+endDate,
		dataType : "json",
		success :function(data) {
				// Remove this as we display it in the badge:
//				$('#countResult').html("No of failures: " + data);
				// Add to the Count Badge
				$('#count').text(data);
				// Show only forst 30 letters of the phone name in the badge:
				$('#ph_model').text(dropdown.substring(0, 30));
			}
		});
}

var getIMSIForFailClass = function(failClass){
	var failCode;
	$('#SEng_table02').DataTable();
	if(failClassDropdown =="EMERGENCY"){
		failCode=0;
	} else if(failClassDropdown == "HIGH PRIORITY ACCESS"){
		failCode=1;
	} else if(failClassDropdown == "MT ACCESS"){
		failCode=2;
	} else if(failClassDropdown == "MO SIGNALLING"){
		failCode=3;
	} else if(failClassDropdown == "MO DATA"){
		failCode=4;
	} else {
		failcode=6;
		alert("invalid");
	}
	$.get({
		url : rootUrl + "getIMSIs/"+failCode,
		dataType : "json",
		success :function(data) {
			$('#SEng_table02').DataTable().clear();
			$.each(data, function(index, object) {
				console.log(object);
				$('#SEng_table02').DataTable().row.add([String(object)]);		 
			});			
			$('#SEng_table02').DataTable().draw();
		}
	});
}

$(document).on("click", "#formBtn", function() {
	var startDate = $('#startDateInput').val();
	var startTime = $('#startTimeInput').val();
	var endDate = $('#endDateInput').val();
	var endTime = $('#endTimeInput').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				getListFailuresBetweenTimes(startDateTimeString, endDateTimeString);
			}else{
				$('#SEng_table01').DataTable().clear();
				var element = document.getElementById("validationAlert");
				element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime">'+
			    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
			    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
				$('#invalidTime').show();
				$('#invalidTime').fadeTo(4000, 500).slideUp(500, function(){
					$('#invalidTime').alert('hide');
	             }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getListFailuresBetweenTimes(startDateTimeString, endDateTimeString);
		}
	}else{
		$('#SEng_table01').DataTable().clear();
		var element = document.getElementById("validationAlert");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate').show();
		$('#invalidDate').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate').alert('hide');
         }); 
	}
	
});

$(document).on("click", "#phoneModelformBtn", function() {
	var startDate = $('#startDateInput1').val();
	var startTime = $('#startTimeInput1').val();
	var endDate = $('#endDateInput1').val();
	var endTime = $('#endTimeInput1').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				var tacNo = $('#phoneModelSelect').val();
				getFailureCountForModel(startDateTimeString, endDateTimeString, tacNo);
			}else{
				$('#countResult').html("No of failures: 0");
				$('#count').text("0");
				var element = document.getElementById("validationAlert2");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime2">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime2').show();
                $('#invalidTime2').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime2').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			var tacNo = $('#phoneModelSelect').val();
			getFailureCountForModel(startDateTimeString, endDateTimeString, tacNo);
		}
	}else{
		$('#countResult').html("No of failures: 0");
		$('#count').text("0");
		var element = document.getElementById("validationAlert2");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate2">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate2').show();
		$('#invalidDate2').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate2').alert('hide');
         }); 
	}
	
});

$(document).on("click", "#formBtnFailClass", function() {
	$('#SEng_table02').DataTable().destroy();
	getIMSIForFailClass(failClassDropdown);
})

$(document).on("click", "#imsiBtn3", function() {
	$('#myTable3').DataTable().destroy();
	var imsi = $('#imsiSelect3 option:selected').text();
	console.log(imsi);
	findByIMSI(imsi);
});

$(document).on("click", "#imsiCauseCodeBtn", function() {
	$('#getCauseCodes1').DataTable().destroy();
	var imsi = $('#imsiSelectForCauseCodes option:selected').text();
	findCauseCodesByIMSI(imsi);
});

$(document).on("click", "#imsiBtn4", function() {
	$('#myTable4').DataTable().destroy();
	var imsi = $('#imsiSelect4 option:selected').text();
	console.log(imsi);
	var startDate = $('#startDateInput4').val();
	var startTime = $('#startTimeInput4').val();
	var endDate = $('#endDateInput4').val();
	var endTime = $('#endTimeInput4').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				console.log(startDateTimeString+"&"+endDateTimeString);
				getCountImsi(imsi, startDateTimeString, endDateTimeString);
			}else{
				$('#myTable4').DataTable().clear();
				$('#count4').text("0");
				var element = document.getElementById("validationAlert4");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime4">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime4').show();
                $('#invalidTime4').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime4').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountImsi(imsi, startDateTimeString, endDateTimeString);
		}
	}else{
		$('#myTable4').DataTable().clear();
		$('#count4').text("0");
		var element = document.getElementById("validationAlert4");
			element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate4">'+
		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
		    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
			$('#invalidDate4').show();
			$('#invalidDate4').fadeTo(4000, 500).slideUp(500, function(){
				$('#invalidDate4').alert('hide');
             }); 
	}
	
});

// clicked Query Variables
var count1 = 2;
var count2 = 2;
var count3 = 2;
var count4 = 2;
var count5 = 2;
var count6 = 2;
var openCloseBox = function() {

	// Click on a side nav link Open the Query Box:
	$(document).on('click', '#left a', function() {

		console.log('...clicked on left nav -> id: ' + this.id);
		
		if (this.id == 'linkQuery1') {
			if (count1 % 2 === 0) {
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse in');
				$('#queryBox1a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count1++;
				console.log('count1:: ' + count1);
			} else {
				console.log('count1:: ' + count1);
				$('#queryBox1').attr('class', 'body collapse');
				$('#queryBox1a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count1++;
				console.log('count1:: ' + count1);
			}
		}
		
		if (this.id == 'linkQuery2') {
			if (count2 % 2 === 0) {
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count2++;
				console.log('count2:: ' + count2);
			} else {
				console.log('count2:: ' + count2);
				$('#queryBox2').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count2++;
				console.log('count2:: ' + count2);
			}
		}
		
		if (this.id == 'linkQuery3') {
			if (count3 % 2 === 0) {
				console.log('count3:: ' + count3);
				$('#queryBox3').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count3++;
				console.log('count3:: ' + count3);
			} else {
				console.log('count3:: ' + count3);
				$('#queryBox3').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count3++;
				console.log('count3:: ' + count3);
			}
		}
		
		if (this.id == 'linkQuery4') {
			if (count4 % 2 === 0) {
				console.log('count4:: ' + count4);
				$('#queryBox4').attr('class', 'body collapse in');
				$('#queryBox4a').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count4++;
				console.log('count4:: ' + count4);
			} else {
				console.log('count4:: ' + count4);
				$('#queryBox4').attr('class', 'body collapse');
				$('#queryBox4a').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count4++;
				console.log('count4:: ' + count4);
			}
		}
		
		if (this.id == 'linkQuery5') {
			if (count5 % 2 === 0) {
				console.log('count5:: ' + count5);
				$('#queryBox5').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count5++;
				console.log('count5:: ' + count5);
			} else {
				console.log('count5:: ' + count5);
				$('#queryBox5').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count5++;
				console.log('count5:: ' + count5);
			}
		}
		
		if (this.id == 'linkQuery6') {
			if (count6 % 2 === 0) {
				console.log('count6:: ' + count6);
				$('#queryBox6').attr('class', 'body collapse in');
				$('.fa-plus').attr('class', 'fa fa-minus');
				count6++;
				console.log('count6:: ' + count6);
			} else {
				console.log('count6:: ' + count6);
				$('#queryBox6').attr('class', 'body collapse');
				$('.fa-minus').attr('class', 'fa fa-plus');
				count6++;
				console.log('count6:: ' + count6);
			}
		}

	});

};

$(document).ready(function(){
	getPhoneModels();
	
	findAllUniqueImsi();
	$(document).on("change", "#phoneModelSelect", function(){
		dropdown = $('#phoneModelSelect :selected').text();
		console.log('...dropdown selected...' + dropdown);
	});
	failClassDropdown="EMERGENCY";
	$(document).on("change", "#selectFailClass", function(){
		failClassDropdown = $('#selectFailClass :selected').text();
		console.log('...dropdown selected...' + failClassDropdown);
	});
	
	// Set all the Query Boxes to Close:
	$('#queryBox1').attr('class', 'body collapse');
	$('#queryBox1a').attr('class', 'body collapse');
	$('#queryBox2').attr('class', 'body collapse');
	$('#queryBox3').attr('class', 'body collapse');
	$('#queryBox4').attr('class', 'body collapse');
	$('#queryBox4a').attr('class', 'body collapse');
	$('#queryBox5').attr('class', 'body collapse');
	$('#queryBox6').attr('class', 'body collapse');
	$('.fa-minus').attr('class', 'fa fa-plus');

	openCloseBox();
	
});

var validateTwoDates = function(startDate,endDate){
	if(startDate>endDate){
		return false
	}else{
		return true;
	}
}

var validateTwoTimes = function(startTime,endTime){
	if(startTime>endTime){
		return false
	}else{
		return true;
	}
}

var findAllUniqueImsi = function(){
	$.get({
		url : "/PiedPiperProject/rest/csr_data/distinctImsi",
		dataType : "json",
		success : populateDropdown
	});
}

var populateDropdown = function(data){
	$.each(data,function(index,basedata){
		$('#imsiSelect3').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		$('#imsiSelect4').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		$('#imsiSelectForCauseCodes').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		
	});
}

var findByIMSI = function(IMSI) {
	if(IMSI !== ""){
	$.ajax({
		type : 'GET',
		url : "/PiedPiperProject/rest/csr_data/" + IMSI,
		dataType : "json",
		success : function(data) {
			$('#myTable3').DataTable().clear();
			$.each(data, function(index, object) {
				 $('#myTable3').DataTable().row.add([object[0], object[1], object[2],object[3]]);
			 });		
			$('#imsiInput').val("");
		}
	});
}
	else{
		alert("No IMSI entered");
	}
}

var getCountImsi = function(imsi, startDate, endDate) {
	 $.get({
		 url : "/PiedPiperProject/rest/csr_data/getCountForIMSI/"+ imsi+"&"+startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#myTable4').DataTable().clear();
			 console.log("cleared")	
			 console.log(data);
			 console.log(imsi+', '+data);
			 
			 // Add to our Count Badge
			 $('#count4').text(data);
			 $('#IMSI').text(imsi);
			 
			 $('#myTable4').DataTable().row.add([imsi, data]);
			 $('#myTable4').DataTable().draw();	
			 console.log("drawn");
		 }
	 });
}

var findCauseCodesByIMSI = function(IMSI){
	console.log(IMSI);
	$.ajax({
		type : 'GET',
		url : '/PiedPiperProject/rest/csr_data/getUniqueCauseCode/'+IMSI,
		dataType : "json",
		success : function(data) {
			$('#getCauseCodes1').DataTable().clear();
			$.each(data, function(index, object) {
				console.log(object);
				$('#getCauseCodes1').DataTable().row.add([object[0],object[1]]);
			});
			$('#getCauseCodes1').DataTable().draw();
		}
	});
}

