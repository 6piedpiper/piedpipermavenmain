var percentageChart1 = function() {

	var table = $('#NE_phoneModelCauseCode').DataTable();

	var eventIDarray = new Array();
	var causeCodeArray = new Array();
	var failureCount = new Array();
	var cellIDFailure = new Array();
	 var cellIDEventCode = new Array();
	var causeCodeArrayWithCount = new Array();
	var data = table.rows().data();

	data.each(function(value, index) {
		var count = $(index).find("td:last-child").html();
		var eventID = $(this).find("td:first").html();
		var description = $(this).find("td:nth-last-child(2)").html();
		var causeCode = $(this).find("td:nth-last-child(3)").html();
		

		failureCount.push(value[3]);
		causeCodeArray.push(value[1]);
		eventIDarray.push(value[0]);

	
	});
	var UniqueEventIDCount = new Array();
	var UniqueEventID = new Array();

	eventIDarray.toString();

	$.each(eventIDarray, function(i, el) {
		if ($.inArray(el, UniqueEventID) === -1)

			UniqueEventID.push(el);
	
	});

	for (var i = 0; i < UniqueEventID.length; i++) {
		UniqueEventIDCount[i] = 0;
	}

	var table = $('#NE_phoneModelCauseCode').DataTable();
	var data = table.rows().data();

	for (var i = 0; i < UniqueEventID.length; i++) {

		data.each(function(value, index) {

			var evid = value[0];

			if (UniqueEventID[i] == evid) {

				UniqueEventIDCount[i] = UniqueEventIDCount[i]
						+ parseInt(value[3]);
			}

		});

	}

	// google.charts.load("current", {packages:["corechart"]});

	
	
	var options = {

		title : 'Call Failures Per Event ID',
		is3D: true,
		legend : {
			textStyle : {
				color : 'black',
				fontSize : 13
			},
			maxLines : 1,
		},
	};
	google.charts.setOnLoadCallback(function() {
		drawMainChart(UniqueEventID, UniqueEventIDCount, options)
	});

	function drawMainChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'EventCode');
		data.addColumn('number', 'Count');

		for (var k = 0; k < UniqueEventID.length; k++) {

			data.addRows([ [ "Event ID: "+UniqueEventID[k] + " ", UniqueEventIDCount[k] ] ]);
		}

		var mainChart = new google.visualization.PieChart(document
				.getElementById('scatter_div'));
		mainChart.draw(data, options);
		google.visualization.events.addListener(mainChart, 'select',
				function() {
					mainChartSelect(mainChart.getSelection()[0].row);
				});
	}

	function drawChildChart(name, data, option) {
		var handledData = new google.visualization.DataTable();
		handledData.addColumn('string', 'name');
		handledData.addColumn('number', 'data');
		for (i = 0; i < name.length; i++) {
			handledData.addRow([ "Cause Code: "+name[i].toString(), data[i] ]);
		}
		var childChart = new google.visualization.PieChart(document
				.getElementById('scatter_div'));
		childChart.draw(handledData, option);
		google.visualization.events.addListener(childChart, 'select',
				childChartSelect);
	}
	
	var UniqueCellID = new Array();
	var UniqueCellFailure = new Array();
	var cellIDFailure = new Array();
	var cellIDEventCode = new Array();
	function mainChartSelect(displayNumber) {
		
		var temp = UniqueEventID[displayNumber];
		var table = $('#NE_phoneModelCauseCode').DataTable();
		
		var data = table.rows().data();
		
		for (var i = 0; i < eventIDarray.length; i++) {

			data.each(function(value, index) {

				var evid = value[0];
				
				var cell2 = value[1];
				if (temp == evid) {
					
					cellIDEventCode.push(cell2);
					
				}
				
				$.each(cellIDEventCode, function(i, el) {
					if ($.inArray(el, UniqueCellID) === -1)
						UniqueCellID.push(el);

				});
			
				
				
				for (var i = 0; i < UniqueCellID.length; i++) {
					UniqueCellFailure[i] = 0;
				}

				
				var table = $('#NE_phoneModelCauseCode').DataTable();
				var data = table.rows().data();
				for (var i = 0; i < UniqueCellID.length; i++) {
					
					data.each(function(value, index) {
						var failure = value[3];
					
						if(value[1] == UniqueCellID[i] && temp == value[0]){
							
							UniqueCellFailure[i]=UniqueCellFailure[i]
							+ parseInt(failure);
							
							
						
						}
						
					});
				}
				
			});
		}
		
		var options = {

				title : 'Call Failures For Event ID: '+temp,
				is3D: true,
				legend : {
					textStyle : {
						color : 'black',
						fontSize : 13
					},
					maxLines : 1,
				},
			};
		
		UniqueCellID.toString();
		drawChildChart(UniqueCellID , UniqueCellFailure, options);
	}

	function childChartSelect() {
		drawMainChart(UniqueCellID, UniqueCellFailure, options);
	}

	// var chart = new google.visualization.PieChart(document
	// .getElementById('scatter_div'));

}
