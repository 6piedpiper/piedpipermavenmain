//All JavaScript for Customer Service Rep queries

var csrRoorUrl = "/PiedPiperProject/rest/csr_data/";

$(document).on("click", "#imsiBtn9", function() {
	var imsi = $('#imsiSelect9 option:selected').text();
	console.log(imsi);
	findByIMSI(imsi);
});

$(document).on("click", "#imsiBtn8", function() {
	$('#myTable8').DataTable().destroy();
	var imsi = $('#imsiSelect8 option:selected').text();
	console.log(imsi);
	var startDate = $('#startDateInput8').val();
	var startTime = $('#startTimeInput8').val();
	var endDate = $('#endDateInput8').val();
	var endTime = $('#endTimeInput8').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				console.log(startDateTimeString+"&"+endDateTimeString);
				getCountImsi(imsi, startDateTimeString, endDateTimeString);
			}else{
				$('#myTable8').DataTable().clear();
				$('#count8').text("0");
				var element = document.getElementById("validationAlert8");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime8">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime8').show();
                $('#invalidTime8').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime8').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getCountImsi(imsi, startDateTimeString, endDateTimeString);
		}
	}else{
		$('#myTable8').DataTable().clear();
		$('#count8').text("0");
		var element = document.getElementById("validationAlert8");
			element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate8">'+
		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
		    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
			$('#invalidDate8').show();
			$('#invalidDate8').fadeTo(4000, 500).slideUp(500, function(){
				$('#invalidDate8').alert('hide');
             }); 
	}
	
});


var findAllUniqueImsi2 = function(){
	$.get({
		url : csrRoorUrl + 'distinctImsi',
		dataType : "json",
		success : populateDropdown
	});
	
}

var findByIMSI = function(IMSI) {
	var localTable = $('#myTable9').DataTable();
	localTable.clear();
	$.ajax({
		type : 'GET',
		url : csrRoorUrl + IMSI,
		dataType : "json",
		success : function(data) {
			console.log(data);
			$.each(data, function(index, object) {
				 localTable.row.add([object[0], object[1], object[2],object[3]]);
				 console.log("Complete");
			 });		
			localTable.draw();
		}
	});
}


var getCountImsi = function(imsi, startDate, endDate) {
	 $.get({
		 url : csrRoorUrl + "getCountForIMSI/"+ imsi+"&"+startDate + "&" + endDate,
		 datatype : "json",
		 success : function(data){
			 $('#myTable8').DataTable().clear();
			 console.log("cleared")	
			 console.log(data);
			 console.log(imsi+', '+data);
			 
			 // Add to our Count Badge
			 $('#count8').text(data);
			 $('#IMSI').text(imsi);
			 
			 $('#myTable8').DataTable().row.add([imsi, data]);
			 $('#myTable8').DataTable().draw();	
			 console.log("drawn");
		 }
	 });
}


var validateTwoDates = function(startDate,endDate){
	if(startDate>endDate){
		return false
	}else{
		return true;
	}
}

var validateTwoTimes = function(startTime,endTime){
	if(startTime>endTime){
		return false
	}else{
		return true;
	}
}

var populateDropdown = function(data){
	$.each(data,function(index,basedata){
		$('#imsiSelect8').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
		$('#imsiSelect9').append('<option value="'+basedata.recordId+'">'+basedata.imso+'</option>');
	});
}

$(document).ready(function(){
	findAllUniqueImsi2();
});