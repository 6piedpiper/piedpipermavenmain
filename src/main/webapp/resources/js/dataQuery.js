//global variables
var startDateTimeString;
var endDateTimeString;
var getCountURL = new URL(
		"http://localhost:8090/PiedPiperProject/rest/Data/getCount/");
var getModelsURL = new URL(
		"http://localhost:8090/PiedPiperProject/rest/Data/getPhoneModels/");
var getListFailuresURL = new URL(
		"http://localhost:8090/PiedPiperProject/rest/Data/getListBetweenTimes/");
var getDataURL = new URL("http://localhost:8090/PiedPiperProject/rest/Data/");
var postSuccess = false;
// get requests
var findByIMSI = function(IMSI) {
	if(IMSI !== ""){
	$.ajax({
		type : 'GET',
		url : '/PiedPiperProject/rest/Data/' + IMSI,
		dataType : "json",
		success : function(data) {
			$('#myTable').DataTable({
				data : data,
						paging : true,
						searching : true,
						sort : true,
						scrollY : 200,
						columns : [ 
						            
							{'data' : 'eventId'},
							{'data' : 'causeCode'},
							{'data' : 'description'},
							],
					});
		}
	});
}
	else{
		alert("No IMSI entered");
	}
}

var findNoFailuresGet = function() {
	$.ajax({
		type : 'GET',
		url : getCountURL,
		dataType : "text",
		success : renderCount,
		error : function() {
			console.log("failed get request");
		}
	});

}

var findPhoneModels = function() {
	$.get({
		url : getModelsURL,
		dataType : "json",
		success : renderDropdown
	});
}

var findNoModelFailures = function(tacNo) {
	$.get({
		url : getCountURL + tacNo,
		dataType : "text",
		success : renderCount
	});
}

var getListFailuresBetweenTimes = function() {
	$.get({
		url : getListFailuresURL,
		dataType : "json",
		success : function(data) {
			// $('#SEng_table01').dataTable().fnDestroy();
			$('#SEng_table01').dataTable(
					{
						data : data,
						paging : true,
						searching : true,
						sort : true,
						scrollY : 200,
						columns : [
								{
									'data' : 'failureDate',
									'render' : function(data) {
										console.log(data);
										var dateTime = new Date(data);
										var prettyDate = dateFormat(dateTime,
												"yyyy mm dd HH:MM:ss");
										return prettyDate;
									}
								}, {
									'data' : 'imso'
								} ],
					});
		}

	});
}

 var getCountImsiAndDuration = function() {
	 $.get({
		 url : getDataURL + "getDistinctImsi",
		 datatype : "json",
		 success : function(data){
			 $('#NE_imsiTable').DataTable().clear();
			 console.log("cleared")	
			 console.log(data);
			 $.each(data, function(index, object) {
				 console.log(object[0]+', '+object[1]+ ', '+object[2]);
				 $('#NE_imsiTable').DataTable().row.add([object[0], object[1], object[2]]);
			 });
			 $('#NE_imsiTable').DataTable().draw();	
			 console.log("drawn");
		 }
	 });
 }

// POST requests
var sendDatesToServer = function() {
	$.ajax({
		type : 'POST',
		url : getCountURL,
		contentType : 'application/json',
		data : JSON.stringify({
			"startDateString" : startDateTimeString,
			"endDateString" : endDateTimeString
		}),
		success : function() {
		},
		error : function() {
			console.log("failed post request");
		}
	});
}

// $('#formID').submit(function(e){
// e.preventDefault();
// extractDateTimes();
// sendDatesToServer();
// getListFailuresBetweenTimes();
// });

var jsonDateObj = function() {
	var string = '{"startDate": "' + startDateTimeString + '","endDate": "'
			+ endDateTimeString + '"}';
	var jsonDate = JSON.parse(string);
	console.log(string);
	return jsonDate;
}

var renderTable = function(data) {
	$('#myTable').dataTable({
		data : data,
				paging : true,
				searching : true,
				sort : true,
				scrollY : 200,
				columns : [ 
				            
					{'data' : 'eventId'},
					{'data' : 'causeCode'}, 
					],
			});
}

var renderCount = function(data) {
	$('#countTable').append(
			'<tr><td>' + startDateTimeString + ' </td><td>' + endDateTimeString
					+ ' </td><td>' + data + ' </td></tr>');
}

var renderDropdown = function(data) {
	$.each(data, function(index, userentity) {
		$('#phoneModelSelect').append(
				'<option value ="' + userentity.tac + '">' + userentity.model
						+ '</option>');
	});
}

var renderList = function(data) {
	$('#countTable').empty();
	$.each(data, function(index, basedata) {
		var dateTime = new Date(basedata.failureDate);
		var prettyDate = dateFormat(dateTime, "yyyy mm dd HH:MM:ss");
		$('#countTable').append(
				'<tr><td>' + prettyDate + '</td><td>' + basedata.imso
						+ ' </td></tr>');
	});
}

var makePrettyDate = function(ulgyDate) {
	console.log(ulgyDate);
	var ulgyNumber = parseInt(ulgyDate)
	var dateTime = new Date(ulgyNumber);
	var prettyDate = dateFormat(dateTime, "yyyy mm dd HH:MM:ss");
	return prettyDate;
}

var extractDateTimes = function() {
	var startDate = $('#startDateInput').val();
	var startTime = $('#startTimeInput').val();
	var endDate = $('#endDateInput').val();
	var endTime = $('#endTimeInput').val();
	startDateTimeString = startDate.toString() + " " + startTime.toString();
	endDateTimeString = endDate.toString() + " " + endTime.toString();
	console.log(startDateTimeString);
	console.log(endDateTimeString);
}

$(document).on("click", "#imsiBtn", function() {
	$('#myTable').DataTable().destroy();
	findByIMSI($('#imsiInput').val());
});

$(document).on("click", "#countModelFailureBtn", function() {
	extractDateTimes();
	var tacNo = $('#phoneModelSelect').val().toString();
	sendDatesToServer();
	console.log(tacNo);
	console.log("successful post")
	findNoModelFailures(tacNo);
});

$(document).on("click", "#formBtn", function() {
	extractDateTimes();
	sendDatesToServer();
	console.log("successful post")
	getListFailuresBetweenTimes();
});

$(document).on("click", "#formBtnNeng", function() {
	extractDateTimes();
	sendDatesToServer();
	console.log("successful post")
	getCountImsiAndDuration();
});

$(document).ready(function() {
	findPhoneModels();
});