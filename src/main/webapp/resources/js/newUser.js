	  var rootUrl="http://localhost:8090/PiedPiperProject/rest/user/newUser";
	  var table;
	  
	  $(document).ready(function() {
		  var today = new Date();
		  $('#date').append('&nbsp;' + dateFormat(today, "dS mmmm, h:MM"));
		  getUsers();
		  $(".alert").hide();
		  $('#newUser').click(function(e){
			  $(".alert").hide();
			  if(($("#userName").val())!="" && $('#pWord').val() !='' && $('#userRole').val()!=''){
				  e.preventDefault();
				  addUser();
			  }
		  })
	 })
	  
	  var getUsers=function(){
	  	console.log('getUsers');
		$.ajax({
			url : "http://localhost:8090/PiedPiperProject/rest/user",
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				table=$('#users').DataTable({
					data : data,
					iDisplayLength: 10,
					columns : [ {
						'data' : 'empId'
					}, {
						'data' : 'userName'
					}, {
						'data' : 'pWord'
					}, {
						'data' : 'userRole'
					}],		
				});
			}
		});
	  }

	  var addUser=function(){
	  	console.log('addUser');
	  	$.ajax({
	  		type:'POST',
	  		contentType:'application/json',
	  		url:'http://localhost:8090/PiedPiperProject/rest/user/newUser',
	  		dataType:'json',
	  		data:formToJSON(),
	  		success: function(response){
	  			clearAll();
	  			var element = document.getElementById("validationAlert");
	  			element.innerHTML = '<div class="alert alert-success fade in" id="userAdded">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Success!</strong> User Created Successfully</p></div>';
	  			$('#userAdded').show();
                $('#userAdded').fadeTo(4000, 500).slideUp(500, function(){
                    $('#userAdded').alert('hide');
                     });  
	  			$('#users').DataTable().destroy();
	  			getUsers();
	  		},
	  		error: function(jqXHR,textStatus,errorThrown){
	  			if(($("#userName").val())!="" && $('#pWord').val() !='' && $('#userRole').val()!=''){
	  				if(($("#userName").val().length<6) || ($("#pWord").val().length<6)){
	  					var element = document.getElementById("validationAlert");
	  					element.innerHTML = '<div class="alert alert-danger" id="userNotAdded">'+
	  				    					'<a href="#" class="close" aria-label="close">&times;</a>'+
	  				    					'<p>Ensure UserName and Password are at least 6 characters long</p></div>';
	  				} else {
	  					var element = document.getElementById("validationAlert");
	  					element.innerHTML = '<div class="alert alert-danger" id="userNotAdded">'+
	    									'<a href="#" class="close" aria-label="close">&times;</a>'+
	    									'<p>Ensure UserName is not already in use</p></div>';
	  				}
	  				$('#userNotAdded').show();
	                $('#userNotAdded').fadeTo(4000, 500).slideUp(500, function(){
	                    $('#userNotAdded').alert('hide');
	                     });  
				  }
	  		}
	  	});
	  }

	  var formToJSON=function(){
		  console.log('stringify');
	  	return JSON.stringify({
	  		"userName":$('#userName').val(),
	  		"pWord":$('#pWord').val(),
	  		"userRole":$('#userRole').val()
	  	})
	  	
	  }
	  
	  var clearAll=function(){
		  console.log('clear')
  			$('#userName').val("");
  			$('#pWord').val("");
  			$('#userRole').val("");
	  }
	  
	  $(function(){
		    $("[data-hide]").on("click", function(){
		        $(this).closest("." + $(this).attr("data-hide")).hide();
		    });
		});
