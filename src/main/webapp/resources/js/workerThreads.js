/**
 * workerThreads.js Creates and starts worker threads for various "fun" Also
 * listens in for responses to events
 */
$(document).ready(function() {
});

if (window.Worker) { //checks browser compatibility
	//create the worker thread
	var worker = new Worker("resources/js/autoFileUploadNotifier.js");
	worker.onmessage = function(e) { // when the worker responds do this
		var result = e.data;
//		alert(result);
		$('.main-bar').after('<div class="fileNote"><h1>'+result+'</h1></div>');
		setTimeout(function(){
			$('.fileNote').remove();
		},3000);
	}

	console.log("polling started");
	var myInterval = setInterval(function() {
		worker.postMessage("go");
		$.finel
	}, 2000); // poll every 2  seconds
	
	document.addEventListener('visibilitychange', function() {
		//when the page becomes hidden then stop polling
		if (document.hidden) {
			clearInterval(myInterval);
			console.log("polling stopped")
		} else {
			console.log("polling resumed")
			myInterval = setInterval(function() {
				worker.postMessage("go");
			}, 1500); // poll every 1.5 seconds

		}
	});
}
