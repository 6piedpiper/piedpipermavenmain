//All JavaScript for Support Engineer queries

	
var rootNengUrl = new URL("http://localhost:8090/PiedPiperProject/rest/seng_data/");
var dropdown;
var getListFailuresBetweenTimes = function(startDate, endDate) {
	$('#SEng_table01').DataTable().destroy();
	$.get({
		url : rootNengUrl+'getListBetweenTimes/' + startDate + "&" + endDate,
		dataType : "json",
		success : function(data) {
			console.log("inside get");
			$('#SEng_table01').DataTable({
				data : data,
				paging : true,
				searching : true,
				sort : true,
				columns : [ {
					'data' : 'failureDate'
				}, {
					'data' : 'imso'
				} ],
			});
		}

	});
}

var getPhoneModels = function(){
	$.get({
		url : rootNengUrl + "getPhoneModels",
		dataType : "json",
		success : function(data){
			$.each(data,function(index,uetype){
		$('#phoneModelSelect').append('<option value="'+uetype.tac+'" selected="selected">'+uetype.model+'('+uetype.manufacturer+')</option>');
			});
		}
	});
}

var getFailureCountForModel = function(startDate, endDate, tacNo){
	$.get({
		url : rootNengUrl + "getCount/"+tacNo+"&"+startDate+"&"+endDate,
		dataType : "json",
		success :function(data) {
				$('#countResult').html("No of failures: " + data);
				// Add to the Count Badge
				$('#countPhoneFailures').text(data);
				// Show only forst 30 letters of the phone name in the badge:
				$('#ph_model').text(dropdown.substring(0, 30));
			}
		});
}

var getIMSIForFailClass = function(failClass){
	var failCode;
	if(failClassDropdown =="EMERGENCY"){
		failCode=0;
	} else if(failClassDropdown == "HIGH PRIORITY ACCESS"){
		failCode=1;
	} else if(failClassDropdown == "MT ACCESS"){
		failCode=2;
	} else if(failClassDropdown == "MO SIGNALLING"){
		failCode=3;
	} else if(failClassDropdown == "MO DATA"){
		failCode=4;
	} else {
		failcode=6;
		alert("invalid");
	}
	$.get({
		url : rootNengUrl + "getIMSIs/"+failCode,
		dataType : "json",
		success :function(data) {
			$('#SEng_table02').DataTable().clear();
			$.each(data, function(index, object) {
				$('#SEng_table02').DataTable().row.add([object.toString()]);		 
			});			
			$('#SEng_table02').DataTable().draw();
		}
	});
}

$(document).on("click", "#formBtn6", function() {
	var startDate = $('#startDateInputSeng2').val();
	var startTime = $('#startTimeInputSeng2').val();
	var endDate = $('#endDateInputSeng2').val();
	var endTime = $('#endTimeInputSeng2').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				getListFailuresBetweenTimes(startDateTimeString, endDateTimeString);
			}else{
				$('#SEng_table01').DataTable().clear();
				var element = document.getElementById("validationAlert6");
				element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime6">'+
			    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
			    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
				$('#invalidTime6').show();
				$('#invalidTime6').fadeTo(4000, 500).slideUp(500, function(){
					$('#invalidTime6').alert('hide');
	             }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			getListFailuresBetweenTimes(startDateTimeString, endDateTimeString);
		}
	}else{
		$('#SEng_table01').DataTable().clear();
		var element = document.getElementById("validationAlert6");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate6">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate6').show();
		$('#invalidDate6').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate6').alert('hide');
         }); 
	}
	
});

$(document).on("click", "#phoneModelformBtn", function() {
	var startDate = $('#startDateInputSeng1').val();
	var startTime = $('#startTimeInputSeng1').val();
	var endDate = $('#endDateInputSeng1').val();
	var endTime = $('#endTimeInputSeng1').val();
	if(validateTwoDates(startDate,endDate)){
		if(startDate==endDate){
			if(validateTwoTimes(startTime,endTime)){
				startDateTimeString = startDate.toString() + " " + startTime.toString();
				endDateTimeString = endDate.toString() + " " + endTime.toString();
				var tacNo = $('#phoneModelSelect').val();
				getFailureCountForModel(startDateTimeString, endDateTimeString, tacNo);
			}else{
				$('#countResult').html("No of failures: 0");
				$('#count').text("0");
				var element = document.getElementById("validationAlert5");
	  			element.innerHTML = '<div class="alert alert-error fade in" id="invalidTime5">'+
	  		    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	  		    					  '<p><strong>Error!</strong> Start Time must preceed End Time</p></div>';
	  			$('#invalidTime5').show();
                $('#invalidTime5').fadeTo(4000, 500).slideUp(500, function(){
                    $('#invalidTime5').alert('hide');
                }); 
			}
		}else{
			startDateTimeString = startDate.toString() + " " + startTime.toString();
			endDateTimeString = endDate.toString() + " " + endTime.toString();
			var tacNo = $('#phoneModelSelect').val();
			getFailureCountForModel(startDateTimeString, endDateTimeString, tacNo);
		}
	}else{
		$('#countResult').html("No of failures: 0");
		$('#count').text("0");
		var element = document.getElementById("validationAlert5");
		element.innerHTML = '<div class="alert alert-error fade in" id="invalidDate5">'+
	    					  '<a href="#" class="close" aria-label="close">&times;</a>'+
	    					  '<p><strong>Error!</strong> Start Date must preceed End Date</p></div>';
		$('#invalidDate5').show();
		$('#invalidDate5').fadeTo(4000, 500).slideUp(500, function(){
			$('#invalidDate5').alert('hide');
         }); 
	}
	
});

$(document).on("click", "#formBtnFailClass", function() {
	$('#SEng_table02').DataTable().destroy();
	getIMSIForFailClass(failClassDropdown);
})

$(document).ready(function(){
	getPhoneModels();
	$(document).on("change", "#phoneModelSelect", function(){
		dropdown = $('#phoneModelSelect :selected').text();
		console.log('...dropdown selected...' + dropdown);
	});
	$(document).on("change", "#selectFailClass", function(){
		failClassDropdown = $('#selectFailClass :selected').text();
		console.log('...dropdown selected...' + failClassDropdown);
	});
});

var validateTwoDates = function(startDate,endDate){
	if(startDate>endDate){
		return false
	}else{
		return true;
	}
}

var validateTwoTimes = function(startTime,endTime){
	if(startTime>endTime){
		return false
	}else{
		return true;
	}
}

