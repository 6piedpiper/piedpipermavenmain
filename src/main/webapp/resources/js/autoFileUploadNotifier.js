/**
 * autoFileUploadNotifier.js sends a request that blocks until a new file is
 * uploaded
 */
onmessage = function(e) {
		
		var xmlhttp = new XMLHttpRequest();
		var url = "http://localhost:8090/PiedPiperProject/rest/import/getNote";

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var fileName = (xmlhttp.responseText);
				postMessage("File " + fileName + " has been successfully uploaded");

			}
		}
		xmlhttp.open("GET",url,true);
		xmlhttp.send();
}
