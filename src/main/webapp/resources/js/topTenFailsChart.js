
var drawBestChart=function(startDate,endDate){
	var chartUrl = "http://localhost:8090/PiedPiperProject/rest/neng_data/";
	var failTable= $('NE_imsiTop10Table').DataTable();
	
	var imsiArray=new Array();
	var failsArray=new Array();
	
		$('#NE_imsiTop10Table tbody tr').each(function() {
			var imsi = $(this).find("td:last-child").html();
			var numFailures = $(this).find("td:first").html();
			imsiArray.push(imsi);
			failsArray.push(numFailures);
		});
		
		google.charts.setOnLoadCallback(drawBestChart);
		
		function drawBestChart() {
			var data = google.visualization.arrayToDataTable([
			   [ 'IMSIs', imsiArray[0], imsiArray[1], imsiArray[2],
			     imsiArray[3], imsiArray[4], imsiArray[5], imsiArray[6],
			     imsiArray[7], imsiArray[8], imsiArray[9]],
			   [ 'IMSI, Count', parseInt(failsArray[0]), parseInt(failsArray[1]),
			     parseInt(failsArray[2]), parseInt(failsArray[3]),
			     parseInt(failsArray[4]), parseInt(failsArray[5]),
			     parseInt(failsArray[6]), parseInt(failsArray[7]),
			     parseInt(failsArray[8]), parseInt(failsArray[9]) ]
			                          	
			   ]);
			
			var options = {
				title : 'Top 10 IMSI Failures for a given time period',
				vAxis : {
					title : 'Failures'
				},
				hAxis : {
					title : 'IMSIs'
				},
				seriesType : 'bars',
				legend: {position: "none"}
			};
			var imsiChart = new google.visualization.ComboChart(document.getElementById('bestBarChart'));
			imsiChart.draw(data, options);
			
			google.visualization.events.addListener(imsiChart, 'select',
					function() {
						var imsi = imsiArray[(imsiChart.getSelection()[0].column)-1];
						imsiChart.setSelection();
						drawBestChartSelect(imsi);	
					});
		}
		
		var eventIdDataArray;
		function drawBestChartSelect(IMSI){
			var eventIdArray = new Array();
			eventIdArray.push(new Array("Event ID","Number of Failures"));
			$.ajax({
				type: 'GET',
				url: chartUrl+"getEventIdWithIMSI/" +IMSI+"&"+startDate+"&"+endDate,
				dataType : "json",
				success : function(data) {
					eventIdDataArray = data;
					$.each(data, function(index, object) {
						var eventId = object[0];
						var eventIDString = "Event ID: "+eventId.toString();
						var count = object[1];
						eventIdArray.push(new Array(eventIDString,count));
					});
					drawEventIDChildChart(eventIdArray, IMSI);
				}
			});
		}
		
		function drawEventIDChildChart(eventIdArray,IMSI){
			var data =  google.visualization.arrayToDataTable(eventIdArray);
			var options = {
					title : 'Event ID Pie Chart for IMSI: '+IMSI,
					pieSliceText : 'label',
					pieStartAngle : 100,
					is3D:true,

				};
			var eventIDPieChart = new google.visualization.PieChart(document
					.getElementById('bestBarChart'));
			eventIDPieChart.draw(data, options);
			google.visualization.events.addListener(eventIDPieChart, 'select',
					function(){
				var eventID = eventIdDataArray[(eventIDPieChart.getSelection()[0].row)][0];
				eventIDPieChartSelect(eventID,IMSI);
			});
		}
		
		function eventIDPieChartSelect(eventID,IMSI){
			var causeCodeArray = new Array();
			causeCodeArray.push(new Array("Description","Number of Failures"));
			$.ajax({
				type: 'GET',
				url: chartUrl+"getBaseDataWithIMSIEventId/"+IMSI+"&"+eventID+"&"+startDate+"&"+endDate,
				dataType: "json",
				success : function(data) {
					$.each(data, function(index, object) {
						var description = object[0]+" "+object[1]+" "+object[2];
						var count = object[3];
						causeCodeArray.push(new Array(description,count));
					});
					drawCauseCodeChart(causeCodeArray, IMSI);
				}
			});
		}
		
		function drawCauseCodeChart(causeCodeArray, IMSI){
			var data =  google.visualization.arrayToDataTable(causeCodeArray);
			var options = {
					title : 'Failure Description Pie Chart for IMSI: '+IMSI,
					pieSliceText : 'label',
					pieStartAngle : 100,
					is3D:true,

				};
			
			var causeCodePieChart = new google.visualization.PieChart(document
					.getElementById('bestBarChart'));
			causeCodePieChart.draw(data, options);
			
			google.visualization.events.addListener(causeCodePieChart, 'select',drawBestChart);
		}
}


		