package com.piedpiper.dataimport;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.piedpiper.data.ImportDAO;


public class DataValidator {
	
	ImportDAO importExcelDao;
	static DataValidator validator;
	int[] CAUSE_CODES;
	int[] MNCs;
	List<Integer[]> eventIdCauseCodes;
	List<Integer[]> mCCAndMNCCombs;
	List<Integer> uETypes;
	
	public static DataValidator getValidatorInstance(ImportDAO excelDao){
		if(validator==null){
			validator = new DataValidator(excelDao);
			return validator;
		}else{
			return validator;
		}
	}
	
	private DataValidator(ImportDAO excelDao){
		importExcelDao = excelDao;
		eventIdCauseCodes = importExcelDao.getEventIdCauseCodes();
		mCCAndMNCCombs = importExcelDao.getMCCAndMNCCombinations();
		uETypes = importExcelDao.getTACNumbers();
	}
	
	public DataValidator(){
		
	}
	
	public int validateBaseDataObject(Row row){
		if(!validateDateAndTime(row)) return 1;
		if(!validateEventIdAndCauseCode(row,eventIdCauseCodes)) return 15;
		if(!validateMCCAndMNC(row, mCCAndMNCCombs)) return 16;
		if(!validateUEType(row, uETypes)) return 4;
		if(!validateFailureClass(row)) return 3;
		if(!validateCellId(row)) return 7;
		if(!validateHierId3(row)) return 12;
		if(!validateHierId32(row)) return 13;
		if(!validateHierId321(row)) return 14;
		if(!validateDuration(row)) return 8;
		if(!validateNEVersion(row)) return 10;
		if(!validateIMSI(row)) return 11;
		
		return -1;
	}
	
	public boolean validateEventIdAndCauseCode(Row row, List<Integer[]> eventIdCauseCodes){
		try{
			int eventId = (int) ExcelReader.getIntValues(row.getCell(1));
		}catch(Exception notANumber){
			return false;
		}
		try{
			int causeCode = (int) ExcelReader.getIntValues(row.getCell(8));
		}catch(Exception notANumber){
			Cell cell = row.getCell(8);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			try{
				String cellString = cell.getStringCellValue();
				if(cellString==null||!cellString.equals("(null)")){
					return false;
				}
			}catch(Exception nullValue){
				return false;
			}
			
		}
		Integer eventId = ExcelReader.getIntValues(row.getCell(1));
		Integer causeCode = ExcelReader.getIntValues(row.getCell(8));
		for(Object[] combinations:eventIdCauseCodes){
			if(causeCode==null) return false;
			if(eventId.equals(combinations[0]) && causeCode.equals(combinations[1])) return true;
		}
		return false;
	
	}
	
	public boolean validateDateAndTime(Row row){
		try{
		Long date = (Long) row.getCell(0).getDateCellValue().getTime();
		}catch(Exception notADate){
			return false;
		}
		Long date = (Long) row.getCell(0).getDateCellValue().getTime();
		if(date > System.currentTimeMillis()){
			return false;
		}
		return true;
	}
	
	public boolean validateMCCAndMNC(Row row, List<Integer[]> mCCAndMNCCombs){
		try{
			int mcc = (int) ExcelReader.getIntValues(row.getCell(4));
			int mnc = (int) ExcelReader.getIntValues(row.getCell(5));
		}catch(Exception notANumber){
			return false;
		}
		Integer mcc = ExcelReader.getIntValues(row.getCell(4));
		Integer mnc = ExcelReader.getIntValues(row.getCell(5));
		for(Object[] rows:mCCAndMNCCombs){
			if(mcc.equals(rows[0])&& mnc.equals(rows[1])) return true;
		}
		return false;
	}
	
	public boolean validateFailureClass(Row row){
		try{
			int failureClass = (int) ExcelReader.getIntValues(row.getCell(2));
		}catch(Exception notANumber){
			Cell cell = row.getCell(2);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			try{
				String cellString = cell.getStringCellValue();
				if(cellString==null||!cellString.equals("(null)")){
					return false;
				}
			}catch(Exception nullValue){
				return false;
			}
			
		}
		
		Integer failureClass = ExcelReader.getIntValues(row.getCell(2));
		if(failureClass==null){
			return true;
		}else{
			for(int i=0;i<5;i++){
				if(i==failureClass) return true;
			}
		}
		return false;
	}
	
	public boolean validateUEType(Row row, List<Integer> uETypes ){
		try{
			Integer uEType = (int)row.getCell(3).getNumericCellValue();
		}catch(Exception notANumber){
			return false;
		}
		Integer uEType = ExcelReader.getIntValues(row.getCell(3));
		if(uETypes.contains(uEType)) return true;
		return false;
	}
	
	public boolean validateCellId(Row row){
		try{
			Integer cellId = (int)ExcelReader.getIntValues(row.getCell(6));
		}catch(Exception notANumber){
			return false;
		}
		
		Integer cellId = ExcelReader.getIntValues(row.getCell(6));
		if(cellId!=null||cellId>0) return true;
		return false;
	}
	
	public boolean validateHierId3(Row row){
		try{
			if(row.getCell(11).getNumericCellValue()>Long.MAX_VALUE) return false;
			long hierId3 = (long)row.getCell(11).getNumericCellValue();
		}catch(Exception notALong){
			return false;
		}
		long hierId3 = (long)row.getCell(11).getNumericCellValue();
		if(hierId3==0||hierId3==-1||String.valueOf(hierId3).length()>19) return false;
		return true;
	}
	
	public boolean validateHierId32(Row row){
		try{
			if(row.getCell(12).getNumericCellValue()>Long.MAX_VALUE) return false;
			long hierId32 = (long)row.getCell(12).getNumericCellValue();
		}catch(Exception notALong){
			return false;
		}
		long hierId32 = (long)row.getCell(12).getNumericCellValue();
		if(hierId32==0||hierId32==-1||String.valueOf(hierId32).length()>19) return false;
		return true;
	}
	
	public boolean validateHierId321(Row row){
		try{
			if(row.getCell(13).getNumericCellValue()>Long.MAX_VALUE) return false;
			long hierId321 = (long)row.getCell(13).getNumericCellValue();
		}catch(Exception notALong){
			return false;
		}
		long hierId321 = (long)row.getCell(13).getNumericCellValue();
		if(hierId321==0||hierId321==-1||String.valueOf(hierId321).length()>19) return false;
		return true;
	}
	
	public boolean validateDuration(Row row){
		try{
			Integer duration = (int)ExcelReader.getIntValues(row.getCell(7));
		}catch(Exception notANumber){
			return false;
		}
		Integer duration = ExcelReader.getIntValues(row.getCell(7));
		if(duration>0||duration!=null) return true;
		return false;
	}
	
	public boolean validateNEVersion(Row row){
		try{
			String neVersion = row.getCell(9).getStringCellValue();
		}catch(Exception notAString){
			return false;
		}
		String neVersion = ExcelReader.getStringValues(row.getCell(9));
		if(neVersion!=null&&neVersion.matches("^[0-9]{2}[A-Ba-b]{1}$")&&neVersion.length()==3){
			return true;
		}
		return false;
	}
	
	public boolean validateIMSI(Row row){
		try{
			long imsi = (long)ExcelReader.getLongValue(row.getCell(10));
		}catch(Exception notALong){
			return false;
		}
		long imsi = ExcelReader.getLongValue(row.getCell(10));
		if(String.valueOf(imsi).length()==15) return true;
		return false;
	}
	
}
