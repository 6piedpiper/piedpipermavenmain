package com.piedpiper.dataimport;

import static com.piedpiper.dataimport.ExcelReader.readExcelFileToDB;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.piedpiper.data.ImportDAO;
/**
 * @author James Runswick
 * @author Date Created 25 May 2016
 * @version 1.0
 *
 */
@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@EJB
	private ImportDAO sampleDatabaseAccess;
	public static List<String> exclusionList = new ArrayList<>();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Part filePart = request.getPart("file"); 
		String disposition = filePart.getHeader("Content-Disposition");
		String fileName = disposition.replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");
		File tmpFile = new File(fileName);
		if(checkFileName(fileName)){
			InputStream fileContent = filePart.getInputStream();
			FileOutputStream fos = new FileOutputStream(fileName);
			int read = 0;
			byte[] bytes = new byte[1_024_000];
			while((read = fileContent.read(bytes)) != -1){
				fos.write(bytes, 0, read);
			}
			fos.close();
			readExcelFileToDB(tmpFile,sampleDatabaseAccess);
			RequestDispatcher rs = request.getRequestDispatcher("dashboard-Admin.html");
			rs.forward(request, response);
		}
	}
	
	private boolean checkFileName(String fileName){
		if(exclusionList.isEmpty()){
			exclusionList.add(fileName);
			return true;
		}else{
			for(String name: exclusionList){
				if(name.equals(fileName)){
					return false;
				}
			}
			exclusionList.add(fileName);
			return true;
		}
	}
	

}
