package com.piedpiper.dataimport;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author James Runswick
 * @author Date Created 6 Jul 2016
 * @version 1.0
 *
 */
@XmlRootElement
public class ImportLog {
	private final Date dateImported;
	private final String filename;
	private final int  successfulImports;
	private final int  failedImports;
	private final long  duration;

	public ImportLog(Date dateImported, String filename, int successfulImports, int failedImports, long duration) {
		super();
		this.dateImported = dateImported;
		this.filename = filename;
		this.successfulImports = successfulImports;
		this.failedImports = failedImports;
		this.duration = duration;
	}

	public String getDateImported() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(dateImported);
	}

	public String getFilename() {
		return filename;
	}

	public int getSuccessfulImports() {
		return successfulImports;
	}

	public int getFailedImports() {
		return failedImports;
	}

	public long getDuration() {
		return duration;
	}

	@Override
	public String toString() {
		return "ImportLog [dateImported=" + dateImported + ", filename=" + filename + ", successfulImports="
				+ successfulImports + ", failedImports=" + failedImports + ", duration=" + duration + " (ms)]";
	}
	
	
}
