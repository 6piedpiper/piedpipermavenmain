package com.piedpiper.dataimport;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.Stack;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.rest.ImportWS;

/**
 * @author James Runswick
 * @author Date Created 28 Jun 2016
 * @version 1.0
 *
 */
public class AutoFileImporter implements Runnable {
	// sets the path based on the system environement variable to allow for
	// operation on mutliple machines
	final String PROJECT_HOME = System.getenv("PROJECT_HOME");
	List<String> exclusions = UploadServlet.exclusionList;
	Path path = Paths.get("src/main/webapp/resources/datasets");
	private static ImportDAO dao;
	private static boolean runService = false;
	private static AutoFileImporter instance = new AutoFileImporter();
	private static Stack<String> uploadedFileNames = ImportWS.uploadedFileNames;

	private AutoFileImporter() {
	}

	public static final void autoImport(ImportDAO newDao) throws IOException {
		if (!runService) {
			dao = newDao;
			runService = true;
			Thread t = new Thread(instance);
			t.start();
		}
	}

	@Override
	public void run() {
		try (WatchService watcher = FileSystems.getDefault().newWatchService();) {

			// define a folder root
			Path dir = Paths.get(PROJECT_HOME + "/src/main/webapp/resources/datasets");
			WatchKey key = dir.register(watcher, ENTRY_CREATE);
			while (runService) {

				// block until a change happens
				System.out.println("AutoFileReader State: READY");
				key = watcher.take();

				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> kind = event.kind();

					if (kind == OVERFLOW)
						continue; // skips if an overflow happens

					@SuppressWarnings("unchecked")
					WatchEvent<Path> ev = (WatchEvent<Path>) event;

					// get filename and create a path to it
					Path filename = ev.context();
					Path child = dir.resolve(filename);
					if (isValidFile(child)) {
						uploadFile(child, dao);
					}
				}

				boolean valid = key.reset(); // start listening again
				if (!valid) {
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			runService = false;
			throw new RuntimeException(e);
		}
	}

	private void uploadFile(Path filepath, ImportDAO importDao) {

		System.out.println(filepath.toString() + " BEGIN UPLOAD");
		File input = new File(filepath.toString());
		ExcelReader.readExcelFileToDB(input, dao);
		exclusions.add(filepath.getFileName().toString());
		uploadedFileNames.push(filepath.getFileName().toString());
	}

	private boolean isValidFile(Path filepath) {
		String incomingFilename = filepath.getFileName().toString();
		return incomingFilename.endsWith(".xls") && !(exclusions.contains(incomingFilename));
	}

}
