package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import java.text.ParseException;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@Entity(name="user_entity")
@NamedQueries({
@NamedQuery(name="user_entity.findAll",query="SELECT u FROM user_entity u"),
@NamedQuery(name="user_entity.findTACno",query="SELECT u.TAC FROM user_entity u"),
@NamedQuery(name="user_entity.findAllBasedOnTac",query="SELECT u FROM user_entity u WHERE u.TAC = :TAC"),
@NamedQuery(name = "user_entity.findTAC", query = "SELECT u.TAC FROM user_entity u WHERE u.model = :model")})
@XmlRootElement
public class UserEntity implements ExcelTabDataObject {
	@Id
	private int TAC;
	private String marketingName;
	private String manufacturer;
	private String accessCapability;
	private String model;
	private String vendorName;
	private String ueType;
	private String operatingSys;
	private String inputMode;

	@Override
	public void RetrieveObjectFromExcel(Row row) throws ParseException {
		for (Cell cell : row) {
			switch (cell.getColumnIndex()) {
			case 0:
				this.TAC = getIntValues(cell);
				break;
			case 1:
				this.marketingName = getStringValues(cell);
			case 2:
				this.manufacturer = getStringValues(cell);
			case 3:
				this.accessCapability = getStringValues(cell);
				break;
			case 4:
				this.model = getStringValues(cell);
				break;
			case 5:
				this.vendorName = getStringValues(cell);
				break;
			case 6:
				this.ueType = getStringValues(cell);
				break;
			case 7:
				this.operatingSys = getStringValues(cell);
				break;
			case 8:
				this.inputMode = getStringValues(cell);
				break;
			}
		}
	}

	/**
	 * @return the tAC
	 */
	public int getTAC() {
		return TAC;
	}

	/**
	 * @param tAC
	 *            the tAC to set
	 */
	public void setTAC(int tAC) {
		TAC = tAC;
	}

	/**
	 * @return the marketingName
	 */
	public String getMarketingName() {
		return marketingName;
	}

	/**
	 * @param marketingName
	 *            the marketingName to set
	 */
	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @param manufacturer
	 *            the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	/**
	 * @return the accessCapability
	 */
	public String getAccessCapability() {
		return accessCapability;
	}

	/**
	 * @param accessCapability
	 *            the accessCapability to set
	 */
	public void setAccessCapability(String accessCapability) {
		this.accessCapability = accessCapability;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the ueType
	 */
	public String getUeType() {
		return ueType;
	}

	/**
	 * @param ueType
	 *            the ueType to set
	 */
	public void setUeType(String ueType) {
		this.ueType = ueType;
	}

	/**
	 * @return the operatingSys
	 */
	public String getOperatingSys() {
		return operatingSys;
	}

	/**
	 * @param operatingSys
	 *            the operatingSys to set
	 */
	public void setOperatingSys(String operatingSys) {
		this.operatingSys = operatingSys;
	}

	/**
	 * @return the inputMode
	 */
	public String getInputMode() {
		return inputMode;
	}

	/**
	 * @param inputMode
	 *            the inputMode to set
	 */
	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}

}
