package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@IdClass(EventCausePK.class)
@NamedQueries({ 
	@NamedQuery(name="EventCause.findAll",query="Select e FROM event_cause e"),
	@NamedQuery(name="EventCause.findDistinctByIMSI",query="Select b FROM BaseData b WHERE b.IMSI = :IMSI"),
	@NamedQuery(name="EventCause.findByIdCauseCode", query="SELECT e FROM event_cause e WHERE e.eventId = :eventId AND e.causeCode = :causeCode"),
	@NamedQuery(name="EventCause.findDescription", query="SELECT e.description FROM event_cause e WHERE e.eventId = :eventId AND e.causeCode = :causeCode")})
@Entity(name ="event_cause")
public class EventCauseData implements ExcelTabDataObject {
	@Id 
	private int causeCode;
	@Id
	private int eventId;
	private String description;

	public EventCauseData() {

	}

	@Override
	public void RetrieveObjectFromExcel(Row row) throws ParseException {
		for (Cell cell: row){
			switch (cell.getColumnIndex()) {
			case 0:
				this.causeCode = getIntValues(cell);
				break;
			case 1:
				this.eventId = getIntValues(cell);
				break;
			case 2:
				this.description = getStringValues(cell);
				break;
			}
		}
	}

	

	/**
	 * @return the causeCode
	 */
	public int getCauseCode() {
		return causeCode;
	}

	/**
	 * @param causeCode the causeCode to set
	 */
	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}

	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof EventCauseData)) return false;
		EventCauseData e = (EventCauseData) obj;
		return (e.causeCode == this.causeCode && e.eventId == this.eventId);
	}
	
	@Override
	public int hashCode(){
		return (this.causeCode + this.eventId); 
	}
}

class EventCausePK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int causeCode;
	int eventId;
	/**
	 * @return the causeCode
	 */
	public int getCauseCode() {
		return causeCode;
	}
	/**
	 * @param causeCode the causeCode to set
	 */
	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}
	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
}
