package com.piedpiper.dataimport.tabObjects;

import java.io.Serializable;

/**
 * @author James Runswick
 * @author Date Created 26 May 2016
 * @version 1.0
 *
 */
public class MachinePK implements Serializable {
	private int mmc;
	private int mnc;

	public MachinePK() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the mmc
	 */
	public int getMmc() {
		return mmc;
	}

	/**
	 * @param mmc
	 *            the mmc to set
	 */
	public void setMmc(int mmc) {
		this.mmc = mmc;
	}

	/**
	 * @return the mnc
	 */
	public int getMnc() {
		return mnc;
	}

	/**
	 * @param mnc
	 *            the mnc to set
	 */
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	@Override
	public int hashCode() {
		return mmc + mnc;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof MachinePK))
			return false;
		MachinePK mpk = (MachinePK) obj;
		return mpk.mmc == mmc && mpk.mnc == mnc;
	}

}
