package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getLongValue;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@XmlRootElement
@Entity(name = "BaseData")
@NamedQueries({ 
		@NamedQuery(name = "BaseData.findAll", query = "Select b FROM BaseData b"),
		@NamedQuery(name = "BaseData.findDistinctUEType", query = "SELECT DISTINCT b.UEType FROM BaseData b"),
		@NamedQuery(name = "BaseData.findDistinctIMSI", query = "Select b FROM BaseData b GROUP BY b.IMSI"),
		@NamedQuery(name = "BaseData.findAllBetTime", query = "Select b FROM BaseData b WHERE b.failureDate BETWEEN :startDate AND :endDate"),
		@NamedQuery(name = "BaseData.findEventIdWithIMSI", query = "Select b.eventId, count(b.eventId) FROM BaseData b WHERE b.IMSI =:IMSI AND b.failureDate BETWEEN :startDate AND :endDate GROUP BY b.eventId"),
		@NamedQuery(name = "BaseData.findAllWithIMSI", query = "SELECT b FROM BaseData b WHERE b.IMSI = :IMSI"),
		@NamedQuery(name = "BaseData.findAllWithIMSIANDEventID", query = "SELECT b.eventId, b.causeCode, count(b.eventId) FROM BaseData b WHERE b.IMSI = :IMSI AND b.eventId = :eventId AND b.failureDate BETWEEN :startDate AND :endDate GROUP BY b.eventId, b.causeCode"),
		@NamedQuery(name = "BaseData.countCallFailuresUEBetTime", query = "SELECT COUNT(b.recordId) FROM BaseData b WHERE b.UEType = :ueType"),
		@NamedQuery(name = "BaseData.countAllBetTime", query = "SELECT COUNT(b.recordId) FROM BaseData b WHERE b.failureDate BETWEEN :startDate AND :endDate"),
		@NamedQuery(name = "BaseData.countImsiSumDuration", query = "SELECT b.IMSI, COUNT(b.IMSI), SUM(b.duration),b.UEType FROM BaseData b WHERE b.failureDate BETWEEN :startDate AND :endDate GROUP BY b.IMSI"),
		@NamedQuery(name = "BaseData.countTop10ImsiFailures", query = "SELECT COUNT(b.IMSI) AS Num_Failures, b.IMSI FROM BaseData b WHERE b.failureDate BETWEEN :startDate AND :endDate GROUP BY b.IMSI ORDER BY Num_Failures DESC"),
		@NamedQuery(name = "BaseData.countTACBetTime", query = "SELECT COUNT(b.UEType) FROM BaseData b WHERE b.UEType = :tacNo AND b.failureDate BETWEEN :startDate AND :endDate"),
		@NamedQuery(name = "BaseData.forIMSIcountAllBetTime", query = "SELECT COUNT(b.recordId) FROM BaseData b WHERE b.IMSI = :imsi AND (b.failureDate BETWEEN :startDate AND :endDate)"),
		@NamedQuery(name = "BaseData.getMaxRecordNo", query = "SELECT MAX(b.recordId) FROM BaseData b"),
		@NamedQuery(name = "BaseData.getDistinctCauseCodeGivenIMSI", query = "SELECT DISTINCT b.eventId, b.causeCode FROM BaseData b WHERE b.IMSI= :IMSI"),
		@NamedQuery(name = "BaseData.getIMSIGivenFailure", query = "SELECT DISTINCT b.IMSI FROM BaseData b WHERE b.failureClass = :failureClass"),
		@NamedQuery(name = "BaseData.findAllWithUEType", query = "SELECT b.eventId, b.causeCode, count(b.eventId) FROM BaseData b WHERE b.UEType = :UEType group by b.eventId, b.causeCode"),
		@NamedQuery(name = "BaseData.countMarketOpCell", query = "SELECT count(b.marketNo) AS Num_Failures,b.marketNo, b.operatorNo, b.cellId  FROM BaseData b WHERE b.failureDate BETWEEN :startDate AND :endDate group by b.marketNo, b.operatorNo, b.cellId ORDER BY Num_Failures DESC")
})
public class BaseData implements ExcelTabDataObject {

	public BaseData() {

	}

	@Id
	private Integer recordId;
	private Timestamp failureDate;
	private Integer eventId;
	private Integer failureClass;
	private Integer UEType;
	private Integer marketNo;
	private Integer operatorNo;
	private Integer cellId;
	private Integer duration;
	private Integer causeCode;
	private String nEVersion;
	private long IMSI;
	private long HIER3_ID;
	private long HIER32_ID;
	private long HIER321_ID;

	/**
	 * Scans through the Row and sets each field based on column number
	 */
	@Override
	public void RetrieveObjectFromExcel(Row row) throws ParseException {
		Cell cell0 = row.getCell(0);
				this.setFailureDate(new Timestamp(cell0.getDateCellValue().getTime()));
		Cell cell1 = row.getCell(1);
				this.eventId = getIntValues(cell1);
		Cell cell2 = row.getCell(2);
				this.failureClass = getIntValues(cell2);
		Cell cell3 = row.getCell(3);
				this.UEType = getIntValues(cell3);
		Cell cell4 = row.getCell(4);
				this.marketNo = getIntValues(cell4);
		Cell cell5 = row.getCell(5);
				this.operatorNo = getIntValues(cell5);
		Cell cell6 = row.getCell(6);
				this.cellId = getIntValues(cell6);
		Cell cell7 = row.getCell(7);
				this.duration = getIntValues(cell7);
		Cell cell8 = row.getCell(8);
				this.causeCode = getIntValues(cell8);
		Cell cell9 = row.getCell(9);
				this.nEVersion = getStringValues(cell9);
		Cell cell10 = row.getCell(10);
				this.IMSI = getLongValue(cell10);
		Cell cell11 = row.getCell(11);
				this.HIER3_ID = getLongValue(cell11);
		Cell cell12 = row.getCell(12);
				this.HIER32_ID = getLongValue(cell12);
		Cell cell13 = row.getCell(13);
				this.HIER321_ID = getLongValue(cell13);
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public Integer getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public int getUEType() {
		return UEType;
	}

	public void setUEType(int uEType) {
		UEType = uEType;
	}

	public int getMarketNo() {
		return marketNo;
	}

	public void setMarketNo(int marketNo) {
		this.marketNo = marketNo;
	}

	public int getOperatorNo() {
		return operatorNo;
	}

	public void setOperatorNo(int operatorNo) {
		this.operatorNo = operatorNo;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Integer getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public String getnEVersion() {
		return nEVersion;
	}

	public void setnEVersion(String nEVersion) {
		this.nEVersion = nEVersion;
	}

	public long getIMSO() {
		return IMSI;
	}

	public void setIMSO(long iMSO) {
		IMSI = iMSO;
	}

	public long getHIER3_ID() {
		return HIER3_ID;
	}

	public void setHIER3_ID(long hIER3_ID) {
		HIER3_ID = hIER3_ID;
	}

	public long getHIER32_ID() {
		return HIER32_ID;
	}

	public void setHIER32_ID(long hIER32_ID) {
		HIER32_ID = hIER32_ID;
	}

	public long getHIER321_ID() {
		return HIER321_ID;
	}

	public void setHIER321_ID(long hIER321_ID) {
		HIER321_ID = hIER321_ID;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public String getFailureDate() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(failureDate);
	}

	public void setFailureDate(Timestamp failureDate) {
		this.failureDate = failureDate;
	}

}