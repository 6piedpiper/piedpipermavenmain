package com.piedpiper.dataimport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataExport.InvalidDataWriter;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.EventCauseData;
import com.piedpiper.dataimport.tabObjects.ExcelTabDataObject;
import com.piedpiper.dataimport.tabObjects.MachineCodeData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 26 May 2016
 * @version 1.0
 *
 */
public final class ExcelReader {
	static List<ImportLog> importLog = new ArrayList<>();
	private static int invalidCount;
	private static int validCount;
	private static long elapsedTime;
	private static List<ExcelTabDataObject> preAddList = new ArrayList<>();

	public final static void readExcelFileToDB(File fileContent, ImportDAO databaseAcesss) {
		elapsedTime = 0;
		 
		long start = System.currentTimeMillis();
		final String PROJECT_HOME = System.getenv("PROJECT_HOME");
		String errorFile;

		if (PROJECT_HOME == null) {

			errorFile = "InvalidData.txt";

		} else {

			errorFile = PROJECT_HOME + "/src/main/webapp/resources/" + "InvalidData.txt";

		}

		createWorkBook(fileContent, databaseAcesss, start, errorFile);
	}

	private static void createWorkBook(File fileContent, ImportDAO databaseAcesss, long start, String errorFile) {
		preAddList.clear();
		validCount = 0;
		 invalidCount = 0;
		try (Workbook wb = WorkbookFactory.create(fileContent)) {

			wb.setSheetOrder("Base Data", wb.getNumberOfSheets() - 1);
			ExcelTabDataObject newDataObject = null;
			
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
				
					if (row.getRowNum() == 0)  {// skip the excel data headers
																	
						if (sheet.getSheetName().equals("Base Data")) {
							InvalidDataWriter.printHeadings(row, errorFile);
						}
						continue;
					}
					newDataObject = createEntityForSheet(newDataObject, sheet);
					if(newDataObject == null){
						continue; 
					}
					if (newDataObject instanceof BaseData) {
						DataValidator validator = DataValidator.getValidatorInstance(databaseAcesss);
						int valid = validator.validateBaseDataObject(row);
						if (valid == -1) {
							newDataObject.RetrieveObjectFromExcel(row);
							preAddList.add(newDataObject);
							validCount++;
							continue;
						} else {
							InvalidDataWriter.writeToFile(row, errorFile, valid);
							invalidCount++;
							continue;
						}
					}
					newDataObject.RetrieveObjectFromExcel(row);
					preAddList.add(newDataObject);
				}
				databaseAcesss.addListOfEntities(preAddList);
			}
			elapsedTime = System.currentTimeMillis() - start;
			ImportLog log = new ImportLog(new Date(), fileContent.getName(), validCount, invalidCount, elapsedTime);
			importLog.add(log);
			System.out.println(log);
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");

		} catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public final static int getValidCount() {
		return validCount;
	}

	public final static long getElapsedTime() {
		return elapsedTime;
	}

	public final static int getInvalidCount() {
		return invalidCount;
	}

	private final static ExcelTabDataObject createEntityForSheet(ExcelTabDataObject newDataObject, Sheet sheet) {
		if (sheet.getSheetName().equals("MCC - MNC Table"))
			return new MachineCodeData();
		if (sheet.getSheetName().equals("UE Table"))
			return new UserEntity();
		if (sheet.getSheetName().equals("Event-Cause Table"))
			return new EventCauseData();
		if (sheet.getSheetName().equals("Base Data"))
			return new BaseData();
		return null;
	}

	public final static Integer getIntValues(Cell cell) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		String intString = cell.getStringCellValue();
		Integer result;
		try {
			result = Integer.parseInt(intString);
		} catch (Exception e) {
			result = null;
		} // end try-catch
		return result;
	}

	public final static String getStringValues(Cell cell) {
		if (cell.getCellType() == Cell.CELL_TYPE_STRING)
			return cell.getStringCellValue();
		return null;
	}

	public final static long getLongValue(Cell cell) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		String longString = cell.getStringCellValue();
		if (longString == null || longString.equals("(null)") || longString.equals("")) {
			return -1;
		}
		return Long.parseLong(longString);
	}

	public static final List<ImportLog> getImportLog() {
		return importLog;
	}
}
