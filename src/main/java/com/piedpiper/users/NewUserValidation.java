package com.piedpiper.users;
 
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
 
@Stateless
@LocalBean
public class NewUserValidation {
     
    public boolean isValid(User user, Query query){
    	System.out.println("validation");
        if((isCorrectLength(user.getUserName(),user.getpWord())) &&  
                (checkDatabase(user.getUserName(), query)==0) &&
                hasUserRole(user.getUserRole())) return true;
        return false;
    }

	private boolean hasUserRole(String userRole) {
		if(userRole.isEmpty()) return false;
		return true;
	}

	private boolean isCorrectLength(String userName, String password){
    	System.out.println("name:"+userName+" password="+password+" check not null");
        if(userName.length()>5 && password.length()>5) return true;
        return false;
    }
     
    @SuppressWarnings("unchecked")
    private int checkDatabase(String userName, Query query){
        List<User> user = null;
        System.out.println("check db "+query.toString());
        query.setParameter(1, userName);
        System.out.println(query.toString());
        user = (List<User>) query.getResultList();
        return user.size();
    }
}