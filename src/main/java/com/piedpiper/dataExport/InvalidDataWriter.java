package com.piedpiper.dataExport;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class InvalidDataWriter {

	public static boolean printHeadings(Row row, String fileName){
		try{
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(fileName, true));
			outputStream.newLine();
			for(int i = 0; i < 14; i++){
				Cell cell = row.getCell(i);
				cell.setCellType(Cell.CELL_TYPE_STRING);
			    String value = cell.getStringCellValue();
				outputStream.write(" | " + value);
			}
			outputStream.newLine();
			outputStream.write("------------------------------------------------------------------------------------------");
			outputStream.write("------------------------------------------------------------------------------------------");
			outputStream.newLine();
			outputStream.flush();
			outputStream.close();
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static void writeToFile(Row row, String fileName, int column){
		try{
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(fileName, true));
			String value = "";
			for(int i = 0; i < 14; i++){
				try{
					Cell cell = row.getCell(i);
					if(i == 0){
						value = cell.getDateCellValue().toString();
						outputStream.write(value);
					} else {
						cell.setCellType(Cell.CELL_TYPE_STRING);
					    value = cell.getStringCellValue();
					    
						outputStream.write(" | " + value);
					}
				}catch(RuntimeException nullValue){
					if(i==0){
						outputStream.write("null");
					}else{
						outputStream.write(" | null");
					}
				}
			}
			outputStream.newLine();
			if(column==15){
				outputStream.write("Error occurred in columns 2 and 9, the "+getColumnName(column));
			}else if(column==16){
				outputStream.write("Error occurred in columns 5 and 6, the "+getColumnName(column));
			}else{
				outputStream.write("Error occurred in column "+column+", the "+getColumnName(column));
			}
			outputStream.newLine();
			outputStream.newLine();
			outputStream.flush();
			outputStream.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static String getColumnName(int columnNum){
		switch(columnNum){
			case 1:
				return "Date/Time Column";
			case 3:
				return "Failure Class Column";
			case 4:
				return "UE Type Column";
			case 7:
				return "Cell ID Column";
			case 8:
				return "Duration Column";
			case 10:
				return "NE Version Column";
			case 11:
				return "IMSI Column";
			case 12:
				return "HierID3 Column";
			case 13:
				return "HierID32 Column";
			case 14:
				return "HierID321 Column";
			case 15:
				return "Event ID and Cause Code Columns";
			case 16:
				return "Market and Operator Columns";
			default:
				return "";
		}
	}
}
