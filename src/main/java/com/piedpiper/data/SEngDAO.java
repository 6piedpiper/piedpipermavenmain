package com.piedpiper.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Stateless
@LocalBean
public class SEngDAO {
	
	@PersistenceContext
	private EntityManager em;

//S-ENG
	public List<BaseData> getAllBetweenTimes(Date startTime, Date endTime) {
		TypedQuery<BaseData> query = em.createNamedQuery("BaseData.findAllBetTime", BaseData.class);
		query.setParameter("startDate", startTime);
		query.setParameter("endDate", endTime);
		return query.getResultList();
	}

	public List<UserEntity> getAllUE() {
		TypedQuery<UserEntity> query = em.createNamedQuery("user_entity.findAll", UserEntity.class);
		List<UserEntity> userEntities = query.getResultList();
		List<UserEntity> tempList = new ArrayList<>();
		TypedQuery<Integer> query2 = em.createNamedQuery("BaseData.findDistinctUEType",Integer.class);
		List<Integer> tacNumbers = query2.getResultList();
		for(UserEntity userEntity: userEntities){
			if(!tacNumbers.contains(userEntity.getTAC())){
				tempList.add(userEntity);
			}
		}
		userEntities.removeAll(tempList);
		return userEntities;
	}

//S-ENG
	public long countAllUEtypeBetweenTimes(Date startDate, Date endDate, String tacNo) {
		TypedQuery<Long> query = em.createNamedQuery("BaseData.countTACBetTime", Long.class);
		Integer tacInt = Integer.parseInt(tacNo);
		query.setParameter("tacNo", tacInt);
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		return query.getSingleResult();
	}
	
	public List<Long> getIMSIGivenFailureClass(int failureClass){
		TypedQuery<Long> query = em.createNamedQuery("BaseData.getIMSIGivenFailure", Long.class);
		query.setParameter("failureClass", failureClass);
		return query.getResultList();
	}
}
