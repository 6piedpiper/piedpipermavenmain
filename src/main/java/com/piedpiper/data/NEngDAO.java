package com.piedpiper.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.EventCauseData;
import com.piedpiper.dataimport.tabObjects.MachineCodeData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Stateless
@LocalBean
public class NEngDAO {

	@PersistenceContext
	private EntityManager em;

	public List<UserEntity> getAllUE() {
		TypedQuery<UserEntity> query = em.createNamedQuery("user_entity.findAll", UserEntity.class);
		List<UserEntity> userEntities = query.getResultList();
		List<UserEntity> tempList = new ArrayList<>();
		TypedQuery<Integer> query2 = em.createNamedQuery("BaseData.findDistinctUEType",Integer.class);
		List<Integer> tacNumbers = query2.getResultList();
		for(UserEntity userEntity: userEntities){
			if(!tacNumbers.contains(userEntity.getTAC())){
				tempList.add(userEntity);
			}
		}
		userEntities.removeAll(tempList);
		return userEntities;
	}
	
	public List<Object[]> countDistinctImsiAndSumDuration(Date startDate, Date endDate) {
		Query query = em.createNamedQuery("BaseData.countImsiSumDuration");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		List<Object[]> firstResult = query.getResultList();
		List<Object[]> outputList = new ArrayList<>();
		TypedQuery<UserEntity> query2 = em.createNamedQuery("user_entity.findAll",UserEntity.class);
		List<UserEntity> userEntities = query2.getResultList();
		for(Object[] data:firstResult){
			for(UserEntity userEntity:userEntities){
				if((int)data[3]==userEntity.getTAC()){
					outputList.add(new Object[]{data[0],data[1],data[2],userEntity.getManufacturer()+" "+userEntity.getModel()});
					break;
				}
			}
			
			
		}
		return outputList;
	}
	
	public List<Object[]> countTop_10_Imsi(Date startDate, Date endDate) {
		Query query = em.createNamedQuery("BaseData.countTop10ImsiFailures");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setMaxResults(10);		
		return query.getResultList();
		
	}

	public List<Object[]> getEventAndCauseGivenUEType(int tac){
		Query query = em.createNamedQuery("BaseData.findAllWithUEType");
		query.setParameter("UEType", tac);
		List<Object[]> list = query.getResultList();
		List<Object[]> output = new ArrayList<>();
		for(Object[] data:list){
			TypedQuery<EventCauseData> query2 = em.createNamedQuery("EventCause.findByIdCauseCode",EventCauseData.class);
			query2.setParameter("eventId", (int)data[0]);
			query2.setParameter("causeCode", (int)data[1]);
			EventCauseData eventCauseObject = query2.getSingleResult();
			output.add(new Object[]{data[0],data[1],eventCauseObject.getDescription(),data[2]});
		}
		return output;
	}

	public List<Object[]> getCountMarketOpCellId(Date startDate, Date endDate){
		Query query = em.createNamedQuery("BaseData.countMarketOpCell");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setMaxResults(10);
		List<Object[]> list = query.getResultList();
		List<Object[]> output = new ArrayList<>();
		Object[] outputObject = new Object[6];
		for(Object[] data: list){
			TypedQuery<MachineCodeData> query2 = em.createNamedQuery("MachineCode.findMachineCode",MachineCodeData.class);
			query2.setParameter("mmc", (int)data[1]);
			query2.setParameter("mnc", (int)data[2]);
			MachineCodeData machineCodeObject = query2.getSingleResult();
			output.add(new Object[]{data[0],data[1],data[2], data[3],machineCodeObject.getCountry(),machineCodeObject.getOperator()});
		}
		return output;
	}
	
	public List<BaseData> getBaseDataForIMSI(Long imsi){
		TypedQuery<BaseData> query = em.createNamedQuery("BaseData.findAllWithIMSI", BaseData.class);
		query.setParameter("IMSI", imsi);
		return query.getResultList();
	}
	
	public List<String> getFailureDescription(Integer eventId, Integer causeCode){
		TypedQuery<String> query = em.createNamedQuery("EventCause.findDescription", String.class);
		query.setParameter("eventId", eventId);
		query.setParameter("causeCode", causeCode);
		return query.getResultList();
	}
	
	public List<Object[]> getBaseDataBetweenDatesWithIMSI(Long imsi, Date startDate, Date endDate){
		Query query = em.createNamedQuery("BaseData.findEventIdWithIMSI");
		query.setParameter("IMSI", imsi);
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		return query.getResultList();
	}
	
	public List<Object[]> getBaseDataWithIMSIEventIDBetTime(Long imsi, int eventId ,Date startDate,Date endDate){
		Query query = em.createNamedQuery("BaseData.findAllWithIMSIANDEventID");
		query.setParameter("IMSI", imsi);
		query.setParameter("eventId", eventId);
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		List<Object[]> list = query.getResultList();
		List<Object[]> output = new ArrayList<>();
		for(Object[] data:list){
			TypedQuery<EventCauseData> query2 = em.createNamedQuery("EventCause.findByIdCauseCode",EventCauseData.class);
			query2.setParameter("eventId", (int)data[0]);
			query2.setParameter("causeCode", (int)data[1]);
			EventCauseData eventCauseObject = query2.getSingleResult();
			output.add(new Object[]{data[0],data[1],eventCauseObject.getDescription(),data[2]});
		}
		return output;
	}
}
