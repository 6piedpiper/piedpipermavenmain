package com.piedpiper.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.piedpiper.dataimport.tabObjects.BaseData;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Stateless
@LocalBean
public class CSRDao {

	@PersistenceContext
	private EntityManager em;

	public List<Object[]> getEventAndCauseGivenIMSI(long imsi){
		List<Object[]> output = new ArrayList<>();
		TypedQuery<BaseData> query = em.createNamedQuery("EventCause.findDistinctByIMSI", BaseData.class);
		query.setParameter("IMSI", imsi);
		List<BaseData> list = query.getResultList();
		for(BaseData data:list){
			Query query2 = em.createNamedQuery("EventCause.findDescription");
			query2.setParameter("eventId", data.getEventId());
			query2.setParameter("causeCode", data.getCauseCode());
			String description = (String) query2.getSingleResult();
			output.add(new Object[]{data.getFailureDate(),data.getEventId(),data.getCauseCode(),description});
		}
		return output;
		
	}

	public long countAllBetweenTimesForIMSI(Date startDate, Date endDate,long imsi){
		TypedQuery<Long> query = em.createNamedQuery("BaseData.forIMSIcountAllBetTime",Long.class);
		query.setParameter("imsi",imsi);
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		long result = query.getSingleResult();
		return result;
	}

	public List<BaseData> getDistinctImsi(){
		TypedQuery<BaseData> query = em.createNamedQuery("BaseData.findDistinctIMSI",BaseData.class);
		return query.getResultList();
	}
	
	public List<Object[]> getDistinctCauseCode(Long imsi){
		List<Object[]> output = new ArrayList<>();
		Query query = em.createNamedQuery("BaseData.getDistinctCauseCodeGivenIMSI");
		query.setParameter("IMSI", imsi);
		List<Object[]> list = query.getResultList();
		for(Object[] data:list){
			Query query2 = em.createNamedQuery("EventCause.findDescription");
			query2.setParameter("eventId", data[0]);
			query2.setParameter("causeCode", data[1]);
			String description = (String) query2.getSingleResult();
			output.add(new Object[]{data[1],description});
		}
		return output;
	}
}

