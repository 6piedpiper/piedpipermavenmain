package com.piedpiper.data;

import java.util.Collection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.ExcelTabDataObject;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Stateless
@LocalBean
public class ImportDAO {

	@PersistenceContext
	private EntityManager em;

	public void addListOfEntities(Collection<ExcelTabDataObject> newObjects) {
		int record = getMaxRecordNo();
		for (ExcelTabDataObject newObject : newObjects) {
			if (newObject instanceof BaseData) { // set the recordNo
				((BaseData) newObject).setRecordId(++record);
			}
			em.merge(newObject);
		}
	}

	public void addSingleEntity(ExcelTabDataObject newObject) {
		em.merge(newObject);
	}

	public List<Integer[]> getEventIdCauseCodes() {
		Query query = em.createQuery("SELECT DISTINCT e.eventId, e.causeCode FROM event_cause e");
		return query.getResultList();
	}

	public List<Integer[]> getMCCAndMNCCombinations() {
		Query query = em.createNamedQuery("MachineCode.findDistinctMMC");
		return query.getResultList();
	}

	public List<Integer> getTACNumbers() {
		Query query = em.createNamedQuery("user_entity.findTACno");
		return query.getResultList();
	}
	private Integer getMaxRecordNo() {
		TypedQuery<Integer> query = em.createNamedQuery("BaseData.getMaxRecordNo", Integer.class);
		Integer maxRecord = query.getSingleResult();
		if (maxRecord == null)
			return new Integer(1);
		return maxRecord;
	}
}
