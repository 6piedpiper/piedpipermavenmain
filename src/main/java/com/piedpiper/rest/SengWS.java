package com.piedpiper.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.piedpiper.data.SEngDAO;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Path("/seng_data")
@Stateless
@LocalBean
public class SengWS {

	@EJB
	private SEngDAO dataAccessor;

	@GET
	@Path("/getCount/{tacNo}&{startDate}&{endDate}")
	@Produces({ MediaType.TEXT_PLAIN })
	public long getUECountBetweenTimes(@PathParam("tacNo") String tacNo,@PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.countAllUEtypeBetweenTimes(startTime, endTime, tacNo);
	}

	@GET
	@Path("/getListBetweenTimes/{startDate}&{endDate}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<BaseData> getListBetweenTimes(@PathParam("startDate") String startDate,@PathParam("endDate") String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.getAllBetweenTimes(startTime, endTime);
	}

	@GET
	@Path("/getPhoneModels")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UserEntity> getAllUE() {
		return dataAccessor.getAllUE();
	}
	
	@GET
	@Path("/getIMSIs/{failureClass}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Long> getIMSIsGivenFailureClass(@PathParam("failureClass") int failureClass) {
		return dataAccessor.getIMSIGivenFailureClass(failureClass);

	}
}
