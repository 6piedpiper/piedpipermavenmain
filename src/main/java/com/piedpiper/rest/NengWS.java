package com.piedpiper.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.piedpiper.data.NEngDAO;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.EventCauseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Path("/neng_data")
@Stateless
@LocalBean
public class NengWS {

	@EJB
	private NEngDAO dataAccessor;

	@GET
	@Path("/getPhoneModels")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UserEntity> getAllUE() {
		return dataAccessor.getAllUE();
	}

	@GET
	@Path("/getDistinctImsi/{startDate}&{endDate}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Object[]> getDistinctImsi(@PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.countDistinctImsiAndSumDuration(startTime, endTime);
	}
	
	@GET
	@Path("/getTop10ImsiFailures/{startDate}&{endDate}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Object[]> getTop_10_Imsi(@PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.countTop_10_Imsi(startTime, endTime);
	}

	@GET
	@Path("/model/{tac}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Object[]> getEventAndCauseIdUEType(@PathParam("tac") int tac) {
		List<Object[]> list = dataAccessor.getEventAndCauseGivenUEType(tac);
		return list;
	}

	@GET
	@Path("/countMarketOpCell/{startDate}&{endDate}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> countMarketOpCell(@PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.getCountMarketOpCellId(startTime, endTime);
	}
	
	@GET
	@Path("/getAllWithIMSI/{imsi}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<BaseData> getAllWithIMSI(@PathParam("imsi") Long imsi){
		List<BaseData> list = dataAccessor.getBaseDataForIMSI(imsi);
		return list;
	}
	
	@GET
	@Path("/getFailDescription/{eventId}&{causeCode}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getAllWithIMSI(@PathParam("eventId") Integer eventId, @PathParam("causeCode") Integer causeCode){
		List<String> list = dataAccessor.getFailureDescription(eventId, causeCode);
		return list;
	}
	
	@GET
	@Path("getEventIdWithIMSI/{imsi}&{startDate}&{endDate}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> getBaseDataWithIMSI(@PathParam("imsi") Long imsi, @PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		List<Object[]> list = dataAccessor.getBaseDataBetweenDatesWithIMSI(imsi, startTime, endTime);
		return list;
	}
	
	@GET
	@Path("getBaseDataWithIMSIEventId/{imsi}&{eventId}&{startDate}&{endDate}")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Object[]> getBaseDataWithIMSIAndEventID(@PathParam("imsi") Long imsi, @PathParam("eventId") int eventId, @PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		List<Object[]> list = dataAccessor.getBaseDataWithIMSIEventIDBetTime(imsi, eventId, startTime, endTime);
		return list;
	}
}
