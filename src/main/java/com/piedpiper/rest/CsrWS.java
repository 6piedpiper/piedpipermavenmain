package com.piedpiper.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.piedpiper.data.CSRDao;
import com.piedpiper.dataimport.tabObjects.BaseData;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Path("/csr_data")
@Stateless
@LocalBean
public class CsrWS {
	
	@EJB
	private CSRDao dataAccessor;

	@GET
	@Path("/getCountForIMSI/{imsi}&{startDate}&{endDate}")
	@Produces({MediaType.APPLICATION_JSON})
	public long getCountForIMSI(@PathParam(value = "imsi") long imsi,@PathParam("startDate") String startDate,@PathParam("endDate")String endDate) throws ParseException{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTime = df.parse(startDate);
		Date endTime = df.parse(endDate);
		return dataAccessor.countAllBetweenTimesForIMSI(startTime, endTime, imsi);
	}

	@Path("/distinctImsi")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<BaseData> getAllDistinctImsi(){
		return dataAccessor.getDistinctImsi();
	}
	//CSR
	@GET
	@Path("/{imsi}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Object[]> getEventAndCauseId(@PathParam("imsi") long imsi) {
		List<Object[]> list = dataAccessor.getEventAndCauseGivenIMSI(imsi);
		return list;
	}
	
	@GET
	@Path("/getUniqueCauseCode/{imsi}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Object[]> getCauseCode(@PathParam("imsi") long imsi) {
		List<Object[]> list = dataAccessor.getDistinctCauseCode(imsi);
		return list;
	}
}
