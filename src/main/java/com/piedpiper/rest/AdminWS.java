package com.piedpiper.rest;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.piedpiper.dataimport.ExcelReader;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Path("/admin")
@Stateless
@LocalBean
public class AdminWS {

	@GET
	@Path("/totalCount")
	@Produces({ MediaType.TEXT_PLAIN})
	public int getTotalToPersist() {
		int totalCount = ExcelReader.getValidCount()+ExcelReader.getInvalidCount();
		return totalCount;
	}
	//admin
	@GET
	@Path("/invalidCount")
	@Produces({ MediaType.TEXT_PLAIN})
	public int getTotalInvalid() {
		int invalidCount = ExcelReader.getInvalidCount();
		return invalidCount;
	}
}
