package com.piedpiper.rest;
 
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
 
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
 
import com.piedpiper.data.UserDAO;
import com.piedpiper.users.NewUserValidation;
import com.piedpiper.users.User;
 
@Path("/user")
@Stateless
@LocalBean
public class UserWebServices {
 
    private NewUserValidation validateUser;
    @EJB
    private UserDAO userDAO;
 
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public List<User> findAllUsers() {
        System.out.println("Get all Users");
        List<User> users = userDAO.getAllUsers();
        System.out.println("....got users....");
        System.out.println(users.size());
        return users;
    }
 
    @POST
    @Path("/login/")
    @Consumes({ MediaType.APPLICATION_JSON })
    public URL findUserNamesPasswords(LoginDetails details) throws MalformedURLException {
        System.out.println("findUserNamesPasswords: " + details.getUsername() + " " + details.getPassword());
        URL userUrl = userDAO.successfulLoginUrl(details.getUsername(), details.getPassword());
        return userUrl;
    }
 
    @GET
    @Path("/find/{query}")
    @Produces({ MediaType.APPLICATION_JSON })
    public List<User> findUserByName(@PathParam("query") String query) {
        System.out.println("findUserByName: " + query);
        List<User> users = userDAO.getUsersByName(query);
        return users;
    }
 
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{empId}")
    public User findUserById(@PathParam("empId") int empId) {
        User user = userDAO.getUserById(empId);
        return user;
 
    }
 
    @POST
    @Path("/newUser/")
    @Consumes("application/json")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response addUser(User user){
        validateUser=new NewUserValidation();
        if(validateUser.isValid(user, userDAO.getQuery())){
        	System.out.println("valid");
        	System.out.println();
            userDAO.add(user);
            return Response.status(201).entity(user).build();
        } else {
        	System.out.println("not valid");
            return Response.status(400).build();
        }
    }
 
    @PUT
    @Consumes("application/json")
    @Produces({ MediaType.APPLICATION_JSON })
    public User updateUser(User user) {
        userDAO.update(user);
        return user;
    }
 
    @DELETE
    @Path("/{empId}")
    public Response deleteUser(@PathParam("empId") int empId) {
        userDAO.delete(empId);
        return Response.status(204).build();
    }

	public void setDAO(UserDAO dao) {
		this.userDAO=dao;
	}
 
}
 
@XmlRootElement
class LoginDetails {
    private String username;
    private String password;
     
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
     
 
}