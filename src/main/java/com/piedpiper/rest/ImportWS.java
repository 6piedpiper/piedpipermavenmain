package com.piedpiper.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataimport.AutoFileImporter;
import com.piedpiper.dataimport.ExcelReader;
import com.piedpiper.dataimport.ImportLog;
import com.piedpiper.dataimport.UploadServlet;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@Path("/import")
@Stateless
@LocalBean
public class ImportWS {

	@EJB
	private ImportDAO dataAccessor;
	public static Stack<String> uploadedFileNames = new Stack<>();

	@GET
	@Path("/fileExclusionList")
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getFileExclusionList(){
		return UploadServlet.exclusionList;
	}

	@Path("/turnOnAuto/")
	@GET
	public boolean startFileAutoChecker(){
		try {
			AutoFileImporter.autoImport(dataAccessor);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@GET
	@Path("/timeForUpload")
	@Produces({MediaType.APPLICATION_JSON})
	public double getTimeForUpload(){
		return (double)ExcelReader.getElapsedTime()/1000;
	}
	
	@GET
	@Path("/importLog")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ImportLog> getImportLog(){
		return ExcelReader.getImportLog();
	}
	
	@GET
	@Path("/getNote")
	@Produces({MediaType.TEXT_PLAIN})
	public String notifyFileUpload(){
		if (uploadedFileNames.isEmpty()){
			return null;
		}else{
			return uploadedFileNames.pop();
		}
	}
}
