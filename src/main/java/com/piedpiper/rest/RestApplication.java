package com.piedpiper.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author James Runswick
 * @author Date Created 3 Jun 2016
 * @version 1.0
 *
 */
@ApplicationPath("/rest")
public class RestApplication extends Application{

}
