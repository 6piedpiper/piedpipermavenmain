package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestMachinePK {

	MachinePK test1, test2, test3;
	
	@Before
	public void setup(){
		test1 = new MachinePK();
		test2 = new MachinePK();
		test3 = new MachinePK();
	}
	
	@Test
	public void testGetsAndSets() {
		test1.setMmc(5);
		test1.setMnc(6);
		assertEquals(5, test1.getMmc());
		assertEquals(6, test1.getMnc());
		assertEquals(11, test1.hashCode());
	}
	
	@Test
	public void testEquals(){
		test1.setMmc(5);
		test1.setMnc(6);
		test2.setMmc(5);
		test2.setMnc(6);
		assertTrue(test1.equals(test2));
		assertTrue(test1.equals(test1));
		assertFalse(test1.equals(new Object()));
		assertFalse(test1.equals(test3));
		test3.setMmc(5);
		assertFalse(test1.equals(test3));
	}

}
