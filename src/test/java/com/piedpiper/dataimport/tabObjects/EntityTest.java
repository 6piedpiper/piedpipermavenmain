package com.piedpiper.dataimport.tabObjects;

import java.text.ParseException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;

import com.piedpiper.dataimport.ExcelReader;

public abstract class EntityTest {

	Row validRow, invalidRow;
	ExcelReader eReader;
	Workbook wb;
	Sheet sheet;

	@Before
	public void setup() {
		eReader = new ExcelReader();
		wb = new HSSFWorkbook();
		sheet = wb.createSheet("new sheet");
		validRow = sheet.createRow((short) 0);
		invalidRow = sheet.createRow((short) 0);
		fillValidRow();
		fillInvalidRow();
	}
	
	abstract void fillValidRow();
	abstract void fillInvalidRow();
	@Test
	abstract public void testGetsAndSets();
	@Test
	abstract public void testRetrieveFromDatabase() throws ParseException;
}
