package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFailureClass {

	FailureClass test;
	
	@Test
	public void testConstructor() {
		test=FailureClass.EMERGENCY;
		assertEquals("EMERGENCY", test.toString());
		assertEquals(0, test.getTableValue());
		assertEquals("EMERGENCY", test.getTableStringEntry());
		
		test=FailureClass.HIGH_PRIORITY_ACCESS;
		assertEquals("HIGH PRIORITY ACCESS", test.toString());
		assertEquals(1, test.getTableValue());
		assertEquals("HIGH PRIORITY ACCESS", test.getTableStringEntry());
		
		test=FailureClass.MT_ACCESS;
		assertEquals("MT ACCESS", test.toString());
		assertEquals(2, test.getTableValue());
		assertEquals("MT ACCESS", test.getTableStringEntry());
		
		test=FailureClass.MO_SIGNALLING;
		assertEquals("MO SIGNALLING", test.toString());
		assertEquals(3, test.getTableValue());
		assertEquals("MO SIGNALLING", test.getTableStringEntry());
		
		test=FailureClass.MO_DATA;
		assertEquals("MO DATA", test.toString());
		assertEquals(4, test.getTableValue());
		assertEquals("MO DATA", test.getTableStringEntry());
	}

}
