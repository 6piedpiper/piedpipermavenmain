package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;

public class TestUserEntity extends EntityTest{

	private UserEntity entity;
	
	@Override
	void fillValidRow() {
		validRow.createCell(0).setCellValue(100);//TAC
		validRow.createCell(1).setCellValue("3310");//MARKET NAME
		validRow.createCell(2).setCellValue("Nokia");//MANUFACTURER
		validRow.createCell(3).setCellValue("GSM 1800");//ACCESS CAPABILITY
		validRow.createCell(4).setCellValue("3310");//MODEL
		validRow.createCell(5).setCellValue("Nokia");//VENDOR NAME
		validRow.createCell(6).setCellValue("HANDHELD");//UE TYPE
		validRow.createCell(7).setCellValue("ANDROID");//OS
		validRow.createCell(8).setCellValue("BASIC");//INPUT MODE
		validRow.createCell(9).setCellValue("");
	}
	
	@Override
	void fillInvalidRow() {

	}

	@Test
	public void testGetsAndSets() {
		entity=new UserEntity();
		entity.setTAC(22);
		entity.setMarketingName("Phone");
		entity.setManufacturer("ERICSSON");
		entity.setAccessCapability("LTE");
		entity.setModel("Phone");
		entity.setVendorName("Vodafone");
		entity.setUeType("HANDHELD");
		entity.setOperatingSys("IOS");
		entity.setInputMode("TOUCH");
		
		assertEquals(22,entity.getTAC());
		assertEquals("Phone",entity.getMarketingName());
		assertEquals("ERICSSON",entity.getManufacturer());
		assertEquals("LTE",entity.getAccessCapability());
		assertEquals("Phone",entity.getModel());
		assertEquals("Vodafone",entity.getVendorName());
		assertEquals("HANDHELD",entity.getUeType());
		assertEquals("IOS",entity.getOperatingSys());
		assertEquals("TOUCH",entity.getInputMode());
	}

	@Test
	@Override
	public void testRetrieveFromDatabase() throws ParseException {
		entity=new UserEntity();
		entity.RetrieveObjectFromExcel(validRow);
		assertEquals(100,entity.getTAC());
		assertEquals("3310",entity.getMarketingName());
		assertEquals("Nokia",entity.getManufacturer());
		assertEquals("GSM 1800",entity.getAccessCapability());
		assertEquals("3310",entity.getModel());
		assertEquals("Nokia",entity.getVendorName());
		assertEquals("HANDHELD",entity.getUeType());
		assertEquals("ANDROID",entity.getOperatingSys());
		assertEquals("BASIC",entity.getInputMode());
	}
}
