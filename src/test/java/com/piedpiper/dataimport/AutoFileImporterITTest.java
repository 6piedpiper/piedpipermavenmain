package com.piedpiper.dataimport;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataExport.InvalidDataWriter;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.rest.ImportWS;

/**
 * @author James Runswick
 * @author Date Created 29 Jun 2016
 * @version 1.0
 *
 */
@RunWith(Arquillian.class)
public class AutoFileImporterITTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(ImportDAO.class,ImportWS.class,InvalidDataWriter.class)
				.addPackages(false,BaseData.class.getPackage(),AutoFileImporter.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}
	
	@EJB
	private ImportDAO testDao;
	private Path testPath;
	final String PROJECT_HOME =System.getenv("PROJECT_HOME");  
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println(PROJECT_HOME);
		
		//start the thread
		AutoFileImporter.autoImport(testDao);
		testPath = Paths.get(PROJECT_HOME +"/src/test/resources/datasets/testData.xls");
	}

	@Test
	@ShouldMatchDataSet("autoTest.yml")
	public void testFileDetectedAndUploadToDatabase() throws IOException, InterruptedException {
		Thread.sleep(500);
		Path targetPath = Paths.get(PROJECT_HOME+"/src/main/webapp/resources/datasets/testData.xls");
		Files.deleteIfExists(targetPath);
		Files.copy(testPath, targetPath);
		Thread.sleep(900);
	}
	
	@Test
	public void testLogIsCorrectSize() throws InterruptedException, IOException {
		Thread.sleep(500);
		Path targetPath = Paths.get(PROJECT_HOME+"/src/main/webapp/resources/datasets/testData.xls");
		Files.deleteIfExists(targetPath);
		Files.copy(testPath, targetPath);
		Thread.sleep(900);
		assertEquals(1,ExcelReader.importLog.size());
	}

}
