package com.piedpiper.dataimport;

import static org.junit.Assert.*;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestDataValidator {

	private static List<Integer[]> eventIdCauseCode;
	private static List<Integer[]> mmcAndMncCodes;
	private static List<Integer> ueTypes;
	private static DataValidator validator;
	static InputStream fin;
	
	
	@BeforeClass
	public static void setUpClass() throws FileNotFoundException {
		eventIdCauseCode = new ArrayList<>();
		mmcAndMncCodes = new ArrayList<>();
		ueTypes = new ArrayList<>();
		validator = new DataValidator();
		fin = new FileInputStream("ValidData/SampleDataset.xls");
		try (Workbook wb = new HSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if(row.getRowNum()==0){
						continue;
					}
					if(sheet.getSheetName().equals("Event-Cause Table")){
						eventIdCauseCode.add(new Integer[]{(int) row.getCell(1).getNumericCellValue(),(int) row.getCell(0).getNumericCellValue()});
					}else if(sheet.getSheetName().equals("MCC - MNC Table")){
						mmcAndMncCodes.add(new Integer[]{(int) row.getCell(0).getNumericCellValue(),(int) row.getCell(1).getNumericCellValue()});
					}else if(sheet.getSheetName().equals("UE Table")){
						ueTypes.add((int) row.getCell(0).getNumericCellValue());
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUp() throws FileNotFoundException{
		fin = new FileInputStream("ValidData/ValidTestData.xlsx");
	}

	@Test
	public void testDataValidatorValidEventIdCauseCode() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateEventIdAndCauseCode(row, eventIdCauseCode));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testDataValidatorValidMMCANDMNC() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateMCCAndMNC(row, mmcAndMncCodes));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDataValidatorValidDateAndTime() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateDateAndTime(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDataValidatorValidUEType() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateUEType(row, ueTypes));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDataValidatorValidCellId() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateCellId(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDataValidatorValidFailureClass() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateFailureClass(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDataValidatorValidHierId3() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateHierId3(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDataValidatorValidHierId32() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateHierId32(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDataValidatorValidHierId321() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateHierId321(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDataValidatorValidDuration() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateDuration(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDataValidatorValidNEVersion() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateNEVersion(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDataValidatorValidIMSI() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertTrue(validator.validateIMSI(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	
	@AfterClass
	public static void tearDown() throws IOException{
		fin.close();
	}

}



