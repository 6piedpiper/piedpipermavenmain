package com.piedpiper.dataimport;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataimport.tabObjects.BaseData;

/**
 * @author James Runswick
 * @author Date Created 27 May 2016
 * @version 1.0
 *
 */
public class ExcelReaderTest {

	public File testFile;
	private ImportDAO mockDAO;
	private DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		File resourcesDirectory = new File("src/test/resources/datasets");
		String address=resourcesDirectory.getAbsolutePath();
		testFile = new File(address + "/testData.xls");
		mockDAO = mock(ImportDAO.class);

	}

	private List<BaseData> setUpTestBaseData() throws ParseException {
		List<BaseData> newList = new ArrayList<>();
		BaseData testData = new BaseData();
		testData.setFailureDate(new Timestamp(df.parse("11/01/2013 17:15").getTime()));
		testData.setEventId(4098);
		testData.setFailureClass(1);
		testData.setUEType(21060800);
		testData.setMarketNo(344);
		testData.setOperatorNo(930);
		testData.setCellId(4);
		testData.setDuration(1000);
		testData.setCauseCode(0);
		testData.setnEVersion("11B");
		testData.setIMSO(344930000000011L);
		testData.setHIER3_ID(4809532081614990000L);
		testData.setHIER32_ID(8226896360947470000L);
		testData.setHIER321_ID(1150444940909480000L);

		newList.add(testData);
		testData = new BaseData();
		testData.setFailureDate(new Timestamp(df.parse("11/01/2013 17:15").getTime()));
		testData.setEventId(4097);
		testData.setFailureClass(1);
		testData.setUEType(21060800);
		testData.setMarketNo(344);
		testData.setOperatorNo(930);
		testData.setCellId(4);
		testData.setDuration(1000);
		testData.setCauseCode(13);
		testData.setnEVersion("11B");
		testData.setIMSO(344930000000011L);
		testData.setHIER3_ID(4809532081614990000L);
		testData.setHIER32_ID(8226896360947470000L);
		testData.setHIER321_ID(1150444940909480000L);

		newList.add(testData);
		return newList;
	}


	@Test
	public void testBaseDataSheet() throws ParseException {
//		final ArgumentCaptor<BaseData> baseDataCatcher = ArgumentCaptor.forClass(BaseData.class);
		// Given
//		final List<BaseData> testData = setUpTestBaseData();
		// When
		doNothing().when(mockDAO).addListOfEntities(any(Collection.class));
		ExcelReader.readExcelFileToDB(testFile, mockDAO);
		// Then
		verify(mockDAO,atLeast(1)).addListOfEntities(any(Collection.class));
//		final List<BaseData> usedObjects = baseDataCatcher.getAllValues();
//		assertThat(testData,is(usedObjects));
		
	}

}
