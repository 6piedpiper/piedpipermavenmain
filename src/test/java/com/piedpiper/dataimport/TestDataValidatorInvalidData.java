package com.piedpiper.dataimport;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestDataValidatorInvalidData {
	
	private static List<Integer[]> eventIdCauseCode;
	private static List<Integer[]> mmcAndMncCodes;
	private static List<Integer> ueTypes;
	private static DataValidator validator;
	static InputStream fin;
	
	
	@BeforeClass
	public static void setUpClass() throws FileNotFoundException {
		eventIdCauseCode = new ArrayList<>();
		mmcAndMncCodes = new ArrayList<>();
		ueTypes = new ArrayList<>();
		validator = new DataValidator();
		fin = new FileInputStream("ValidData/SampleDataset.xls");
		try (Workbook wb = new HSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if(row.getRowNum()==0){
						continue;
					}
					if(sheet.getSheetName().equals("Event-Cause Table")){
						eventIdCauseCode.add(new Integer[]{(int) row.getCell(1).getNumericCellValue(),(int) row.getCell(0).getNumericCellValue()});
					}else if(sheet.getSheetName().equals("MCC - MNC Table")){
						mmcAndMncCodes.add(new Integer[]{(int) row.getCell(0).getNumericCellValue(),(int) row.getCell(1).getNumericCellValue()});
					}else if(sheet.getSheetName().equals("UE Table")){
						ueTypes.add((int) row.getCell(0).getNumericCellValue());
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void setup() throws FileNotFoundException{
		fin = new FileInputStream("InvalidTestData/InvalidTestingData.xlsx");
	}
	
	@Test
	public void testInvalidDatesAndTimes() throws FileNotFoundException{
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateDateAndTime(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testInvalidNEVersion() throws FileNotFoundException{
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateNEVersion(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testInvalidIMSI() throws FileNotFoundException{
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateIMSI(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidUEType() throws FileNotFoundException{
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateUEType(row,ueTypes));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testInvalidCellIds() throws FileNotFoundException{
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateCellId(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testInvalidEventIdCauseCode() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateEventIdAndCauseCode(row, eventIdCauseCode));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testInvalidMMCANDMNC() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateMCCAndMNC(row, mmcAndMncCodes));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidFailureClass() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateFailureClass(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidHierId3() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateHierId3(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidHierId32() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateHierId32(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidHierId321() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateHierId321(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInvalidDuration() throws FileNotFoundException {
		try (Workbook wb = new XSSFWorkbook(fin);) {
			for (Sheet sheet : wb) {
				for (Row row : sheet) {
					if (row.getRowNum() == 0) {
						continue;
					}
					assertFalse(validator.validateDuration(row));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() throws IOException{
		fin.close();
	}
}
