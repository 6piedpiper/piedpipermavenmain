package com.piedpiper.dataExport;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class InvalidDataWriterTest {
	
	Row testRow;
	Cell testCell;
	@Before
	public void setup(){
		testRow = mock(Row.class);
		testCell = mock(Cell.class);
	}
	
	
	@Parameters
	private static final Object[] getValidInput(){
		return $(
				$(1,"Date/Time Column"),
				$(3,"Failure Class Column"),
				$(4,"UE Type Column"),
				$(7,"Cell ID Column"),
				$(8,"Duration Column"),
				$(10,"NE Version Column"),
				$(11,"IMSI Column"),
				$(12,"HierID3 Column"),
				$(13,"HierID32 Column"),
				$(14,"HierID321 Column"),
				$(15,"Event ID and Cause Code Columns"),
				$(16,"Market and Operator Columns"));
				
	}
	
	@Parameters
	private static final Object[] getInvalidInput(){
		return $(
				$(-1,""),
				$(17,""));
	}
	
	@Test
	@Parameters(method="getValidInput")
	public void testValidInput(int columnNum, String columnName){
		assertEquals(columnName, InvalidDataWriter.getColumnName(columnNum));
	}
	
	@Test
	@Parameters(method="getInvalidInput")
	public void testInvalidInput(int columnNum, String columnName){
		assertEquals(columnName, InvalidDataWriter.getColumnName(columnNum));
	}
	
	@Test
	public void testPrintHeadings(){
		when(testRow.getCell(anyInt())).thenReturn(testCell);
		when(testCell.getStringCellValue()).thenReturn("Test Success");
		assertTrue(InvalidDataWriter.printHeadings(testRow,"invalidFileTest.txt"));
		
	}
	
}
