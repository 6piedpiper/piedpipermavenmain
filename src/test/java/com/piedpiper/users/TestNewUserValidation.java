package com.piedpiper.users;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import javax.persistence.Query;

public class TestNewUserValidation {

	User user;
	NewUserValidation test;
	Query query;
	List<User> userList;
	
	@Before
	public void setup(){
		user=new User();
		test=new NewUserValidation();
		query=mock(Query.class);
		userList=new ArrayList<User>();
	}
	
	@Test
	public void testValidUser() {
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("tester");
		user.setpWord("tester");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertTrue(test.isValid(user, query));
	}
	
	@Test
	public void testInvalidNameLength(){
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("test");
		user.setpWord("tester");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertFalse(test.isValid(user, query));
	}
	
	@Test
	public void testInvalidPasswordLength(){
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("tester");
		user.setpWord("test");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertFalse(test.isValid(user, query));
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullUserRole(){
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("tester");
		user.setpWord("tester");
		test.isValid(user, query);
	}
	
	@Test
	public void testNameInUse(){
		User user2=new User();
		user.setEmpId(1);
		user.setUserName("tester");
		user.setpWord("tester");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		userList.add(user2);
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("tester");
		user.setpWord("testing");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertFalse(test.isValid(user, query));
	}

	@Test
	public void testPasswordInUse(){
		User user2=new User();
		user2.setEmpId(1);
		user2.setUserName("tester");
		user2.setpWord("tester");
		user2.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		userList.add(user2);
		when(query.getResultList()).thenReturn(userList);
		user.setEmpId(1);
		user.setUserName("testing");
		user.setpWord("tester");
		user.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertFalse(test.isValid(user, query));
	}
}
