package com.piedpiper.users;


import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class TestCreateNewUserSelenium {
	
		  
		  private WebDriver driver;
		  private ScreenshotHelper screenshotHelper;
		  
		  @Before
		  public void openBrowser() {
		   
		    System.setProperty("webdriver.chrome.driver", "C:/Users/A00190744/chromedriver.exe");
		    driver = new ChromeDriver();
		    
		    screenshotHelper = new ScreenshotHelper();
		  }
		  
		  @After
		  public void saveScreenshotAndCloseBrowser() throws IOException {
		    screenshotHelper.saveScreenshot("screenshot.png");
		    driver.quit();
		  }
		  
	
		  
		  private class ScreenshotHelper {
		  
		    public void saveScreenshot(String screenshotFileName) throws IOException {
		      File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		      FileUtils.copyFile(screenshot, new File(screenshotFileName));
		    }
		  }

		  

	@Test
	public void testSuccessfulCreateNewUser() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/dashboard-Admin.html");
	
		WebElement userNameInput = driver.findElement(By.id("userName"));
		userNameInput.clear();
		userNameInput.sendKeys("NewUser6Letter");
		
		WebElement passwordInput = driver.findElement(By.id("pWord"));
		passwordInput.clear();
		passwordInput.sendKeys("Password1");
		
		WebElement element=driver.findElement(By.id("userRole"));
		Select se=new Select(element);
		se.selectByIndex(1);
		
		
		WebElement click = driver.findElement(By.xpath("//*[@id='newUser']"));
		
		click.click();
		Thread.sleep(5000);
		Alert alert = driver.switchTo().alert();
		
		driver.switchTo().alert().accept();
		assertEquals("User created successfully",alert.getText());
		Thread.sleep(5000);
		assertEquals("Dashboard - System Admin", driver.getTitle());
		
	
	}

	@Test
	public void testFailedCreateNewUser() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/dashboard-Admin.html");
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		userNameInput.clear();
		
		WebElement passwordInput = driver.findElement(By.id("pWord"));
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		WebElement element=driver.findElement(By.id("userRole"));
		Select se=new Select(element);
		se.selectByIndex(1);
		
		
		WebElement click = driver.findElement(By.xpath("//*[@id='newUser']"));
		
		
		
		click.click();
		Thread.sleep(5000);
		Alert alert = driver.switchTo().alert();
		
		driver.switchTo().alert().accept();
		assertEquals("addUser error: error",alert.getText());
		Thread.sleep(5000);
		assertEquals("Dashboard - System Admin", driver.getTitle());
		
	
	}
	
}