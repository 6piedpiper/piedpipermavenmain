package com.piedpiper.data;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(Arquillian.class)
public class TestSuccessfulLoginUrlITTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true,Filters.exclude(".*Test.*"),"com.piedpiper")
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
//				.addAsWebInfResource("jbossas-ds.xml")//uncomment for this test to work
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}

	@EJB
	private UserDAO userDAO;
	private URL url=null;
	
	
	@Test
	@UsingDataSet({ "users.yml" })
	public void testSuccessAdmin() throws MalformedURLException{
		url=new URL("http://localhost:8090/PiedPiperProject/dashboard-Admin.html");
		assertEquals(url,userDAO.successfulLoginUrl("Admin1", "Admin1"));
	}
	
	@Test
	@UsingDataSet({ "users.yml" })
	public void testSuccessCSR() throws MalformedURLException{
		url=new URL("http://localhost:8090/PiedPiperProject/dashboard-CSR.html");
		assertEquals(url,userDAO.successfulLoginUrl("CSRTest", "CSRTest"));
	}
	
	@Test
	@UsingDataSet({ "users.yml" })
	public void testSuccessSEng() throws MalformedURLException{
		url=new URL("http://localhost:8090/PiedPiperProject/dashboard-SEng.html");
		assertEquals(url,userDAO.successfulLoginUrl("SEngTest", "SEngTest"));
	}
	
	@Test
	@UsingDataSet({ "users.yml" })
	public void testSuccessNEng() throws MalformedURLException{
		url=new URL("http://localhost:8090/PiedPiperProject/dashboard-NEng.html");
		assertEquals(url,userDAO.successfulLoginUrl("NEngTest", "NEngTest"));
	}
	
	@Test
	@UsingDataSet({ "users.yml" })
	public void testUnsuccessful() throws MalformedURLException{
		url=new URL("http://localhost:8090/PiedPiperProject/index.html");
		assertEquals(url,userDAO.successfulLoginUrl("incorrect", "incorrect"));
	}
	
}
