package com.piedpiper.data;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.UserDAO;
import com.piedpiper.users.User;
import com.piedpiper.users.UserRole;
@RunWith(Arquillian.class)
public class UserDAOITTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true,Filters.exclude(".*Test.*"),"com.piedpiper")
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
//				.addAsWebInfResource("jbossas-ds.xml")//uncomment for this test to work
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}

	@EJB
	private UserDAO userDAO;
	
	private User user;
	private List<User> userList;
	
	@Before
	public void setup() {
		user = new User();
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testGetAllUsers() {
		userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 4);
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testSearchById() {
		userList = userDAO.getAllUsers();
		int id = userList.get(0).getEmpId();
		user = userDAO.getUserById(id);
		assertEquals(user.getUserName(), "Admin1");
		assertEquals(user.getpWord(), "Admin1");
		
		user = userDAO.getUserById(20);
		assertNull(user);
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testSearchByName() {
		userList= userDAO.getUsersByName("Admin1");
		assertEquals("Data fetch = data persisted", userList.size(), 1);

		userList = userDAO.getUsersByName("NoUser");
		assertEquals("Data fetch = data persisted", userList.size(), 0);
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testAddUser() {
		user = new User();
		userDAO.add(user);
		userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 5);
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testUpdateUser() {
		User user = new User();
		user.setEmpId(66);
		userDAO.add(user);
		user.setUserRole(UserRole.SUPPORT_ENGINEER);
		userList = userDAO.getUsersByName("NewUser");
		assertEquals(userList.size(),0);
		user.setUserName("NewUser");
		user.setpWord("NewUser1");
		userDAO.update(user);
		userList = userDAO.getUsersByName("NewUser");
		assertEquals(1, userList.size());
	}

	@Test
	@UsingDataSet({ "users.yml" })
	public void testDeleteUser() {
		user = new User();
		userDAO.add(user);
		userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 5);
		int id = userList.get(1).getEmpId();
		userDAO.delete(id);
		userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 4);
	}

}
