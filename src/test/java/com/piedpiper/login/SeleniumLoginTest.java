package com.piedpiper.login;
import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;


public class SeleniumLoginTest {
	
		  
		  private WebDriver driver;
		  private ScreenshotHelper screenshotHelper;
		  
		  @Before
		  public void openBrowser() {
		   
		    System.setProperty("webdriver.chrome.driver", "C:/Users/A00190744/chromedriver.exe");
		    driver = new ChromeDriver();
		    
		    screenshotHelper = new ScreenshotHelper();
		  }
		  
		  @After
		  public void saveScreenshotAndCloseBrowser() throws IOException {
		    screenshotHelper.saveScreenshot("screenshot.png");
		    driver.quit();
		  }
		  
	
		  
		  private class ScreenshotHelper {
		  
		    public void saveScreenshot(String screenshotFileName) throws IOException {
		      File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		      FileUtils.copyFile(screenshot, new File(screenshotFileName));
		    }
		  }

		  
	@Test	  
	public void testPageTitle(){
		

		driver.get("http://localhost:8090/PiedPiperProject/");
		assertEquals("Login Manager", driver.getTitle());
	}
	
	@Test
	public void testAdminLogin() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/");
		
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		assertEquals(true, userNameInput.isDisplayed());
		userNameInput.clear();
		userNameInput.sendKeys("admin");

		
		WebElement passwordInput = driver.findElement(By.id("password"));
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		WebElement click = driver.findElement(By.xpath("//*[@id='buttonSubmit']"));
		
		click.click();
		Thread.sleep(5000);
		assertEquals("Dashboard - System Admin", driver.getTitle());

	}
	
	@Test
	public void testSupportEngineerLogin() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/");
		
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		assertEquals(true, userNameInput.isDisplayed());
		userNameInput.clear();
		userNameInput.sendKeys("seng");

		
		WebElement passwordInput = driver.findElement(By.id("password"));
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		WebElement click = driver.findElement(By.xpath("//*[@id='buttonSubmit']"));
		
		click.click();
		Thread.sleep(5000);
		assertEquals("Dashboard - Support Engineering", driver.getTitle());

	}
	
	@Test
	public void testNetworkEngineerLogin() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/");
		
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		assertEquals(true, userNameInput.isDisplayed());
		userNameInput.clear();
		userNameInput.sendKeys("neng");

		
		WebElement passwordInput = driver.findElement(By.id("password"));
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		WebElement click = driver.findElement(By.xpath("//*[@id='buttonSubmit']"));
		
		click.click();
		Thread.sleep(5000);
		assertEquals("Dashboard - Network Management", driver.getTitle());

	}
	
	@Test
	public void testCustomerServiceRepLogin() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/");
		
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		assertEquals(true, userNameInput.isDisplayed());
		userNameInput.clear();
		userNameInput.sendKeys("rep");

		
		WebElement passwordInput = driver.findElement(By.id("password"));
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		WebElement click = driver.findElement(By.xpath("//*[@id='buttonSubmit']"));
		
		click.click();
		Thread.sleep(8000);
		assertEquals("Dashboard - Customer Service", driver.getTitle());

	}

	@Test
	public void testFailedLogin() throws InterruptedException{
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		driver.get("http://localhost:8090/PiedPiperProject/");
		
		
		WebElement userNameInput = driver.findElement(By.id("userName"));
		assertEquals(true, userNameInput.isDisplayed());
		userNameInput.clear();
		userNameInput.sendKeys("invalid");


		WebElement passwordInput = driver.findElement(By.id("password"));
		assertEquals(true, passwordInput.isDisplayed());
		passwordInput.clear();
		passwordInput.sendKeys("000");
		
		
		WebElement click = driver.findElement(By.xpath("//*[@id='buttonSubmit']"));
		
		click.click();
		
		assertEquals("Login Manager", driver.getTitle());
	}


	
}