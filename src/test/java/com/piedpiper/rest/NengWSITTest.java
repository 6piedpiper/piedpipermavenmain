package com.piedpiper.rest;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.NEngDAO;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@RunWith(Arquillian.class)
public class NengWSITTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(NengWS.class,NEngDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
//				.addAsWebInfResource("jbossas-ds.xml");
		return testWar;
	}
	
	@EJB private NengWS testDataIn;

	@Test
	@UsingDataSet({"user_entity.yml", "basedata.yml"})
	public void testGetAllUESize() {
		List<UserEntity> testList = testDataIn.getAllUE();
		assertEquals(1,testList.size());
	}
	
	@Test
	@UsingDataSet({"EventCauseData.yml","basedata.yml"})
	public void testGetEventCauseForPhoneModelSize() {
		List<Object[]> testList = testDataIn.getEventAndCauseIdUEType(21060800);
		assertEquals(3,testList.size());
	}

	/**
	 * Test method for {@link com.piedpiper.rest.DataQueryEJB#getDistinctImsi()}.
	 * @throws ParseException 
	 */
	@Test
	@UsingDataSet({"basedata.yml","user_entity.yml"})
	public void testGetDistinctImsiListSize() throws ParseException {
		List<Object[]> queryResult = testDataIn.getDistinctImsi("2013-01-11 00:00","2013-01-12 00:00");
		assertEquals(1,queryResult.size());
	}
	
	@Test
	@UsingDataSet({"machine_code.yml","basedata.yml"})
	public void testCountTop10MarketOpCellId() throws ParseException {
		List<Object[]> testList = testDataIn.countMarketOpCell("2013-01-11 00:00","2013-01-12 00:00");
		assertEquals(3,testList.size());
	}
	
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testTop10ImsiFailures() throws ParseException {
		List<Object[]> testList = testDataIn.getTop_10_Imsi("2013-01-11 00:00","2013-02-23 00:00");
		assertEquals(1,testList.size());
	}
	
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetBaseDataBetweenDatesWithIMSI() throws ParseException {
		List<Object[]> testList = testDataIn.getBaseDataWithIMSI(344930000000011L,"2013-01-11 00:00","2013-02-23 00:00");
		assertEquals(2,testList.size());
	}
	
	@Test
	@UsingDataSet({"basedata.yml","EventCauseData.yml"})
	public void testGetBaseDataWithIMSIAndEventID() throws ParseException {
		List<Object[]> testList = testDataIn.getBaseDataWithIMSIAndEventID(344930000000011L,4098,"2013-01-11 00:00","2013-02-23 00:00");
		assertEquals(2,testList.size());
	}
}
