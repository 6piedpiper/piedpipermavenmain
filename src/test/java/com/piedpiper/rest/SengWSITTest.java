package com.piedpiper.rest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.SEngDAO;
import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

@RunWith(Arquillian.class)
public class SengWSITTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(SengWS.class,SEngDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
//				.addAsWebInfResource("jbossas-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}
	
	@EJB private SengWS testDataIn;

	@Test
	@UsingDataSet({"user_entity.yml", "basedata.yml"})
	public void testGetAllUESize() {
		List<UserEntity> testList = testDataIn.getAllUE();
		assertEquals(1,testList.size());
	}
	
	/**
	 * Test method for {@link com.piedpiper.rest.DataQueryEJB#getUECountBetweenTimes(java.lang.String)}.
	 * @throws ParseException 
	 */
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetUECountBetweenTimesSize() throws ParseException {
		long count = testDataIn.getUECountBetweenTimes("21060800","2013-01-11 00:00","2013-01-12 00:00");
		assertEquals(4L,count);
	}

	/**
	 * Test method for {@link com.piedpiper.rest.DataQueryEJB#getListBetweenTimes()}.
	 * @throws ParseException 
	 */
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetListBetweenTimes() throws ParseException {
		int count = testDataIn.getListBetweenTimes("2013-01-11 00:00","2013-01-12 00:00").size();
		assertEquals(4,count);
	}
	
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetDistinctIMSI() throws ParseException {
		List<Long> imsis=testDataIn.getIMSIsGivenFailureClass(0);
		assertEquals(0,imsis.size());
		
		imsis=testDataIn.getIMSIsGivenFailureClass(1);
		assertEquals(1,imsis.size());
	}

}
