package com.piedpiper.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataExport.InvalidDataWriter;
import com.piedpiper.dataimport.ExcelReader;
import com.piedpiper.dataimport.tabObjects.BaseData;

@RunWith(Arquillian.class)
public class ImportWSITTest {


	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(ImportWS.class,ImportDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addPackage(ExcelReader.class.getPackage())
				.addPackage(InvalidDataWriter.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}
	
	
	@EJB private ImportWS testDataIn;
	
	@Test
	public void testGetFileExclusionList() {
		assertEquals(0,testDataIn.getFileExclusionList().size());
	}

	@Test
	public void testStartFileAutoChecker() {
		assertTrue(testDataIn.startFileAutoChecker());
	}

}
