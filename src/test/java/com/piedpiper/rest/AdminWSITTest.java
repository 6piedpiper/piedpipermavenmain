package com.piedpiper.rest;

import static org.junit.Assert.assertEquals;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.ImportDAO;
import com.piedpiper.dataExport.InvalidDataWriter;
import com.piedpiper.dataimport.ExcelReader;
import com.piedpiper.dataimport.tabObjects.BaseData;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@RunWith(Arquillian.class)
public class AdminWSITTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(AdminWS.class,ImportDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addPackage(ExcelReader.class.getPackage())
				.addPackage(InvalidDataWriter.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}
	
	@EJB private AdminWS testDataIn;
	
	@Test
	public void testGetTotalToPersist() {
		assertEquals(0,testDataIn.getTotalToPersist());
	}
	
	@Test
	public void testGetTotalInvalid() {
		assertEquals(0,testDataIn.getTotalInvalid());
	}
}
