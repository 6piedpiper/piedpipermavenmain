package com.piedpiper.rest;

import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.verification.Times;

import com.piedpiper.data.UserDAO;
import com.piedpiper.users.NewUserValidation;
import com.piedpiper.users.User;

public class TestUserWebServices {

	UserDAO dao;
	UserWebServices uws;
	User user;
	NewUserValidation valid;
	List<User> users;
	
	@Before
	public void setup(){
		dao=mock(UserDAO.class);
		uws=new UserWebServices();
		uws.setDAO(dao);
		user=new User();
		users=new ArrayList<User>();
		users.add(user);
		valid=mock(NewUserValidation.class);
	}
	
	@Test
	public void testGetAll() {
		when(dao.getAllUsers()).thenReturn(users);
		uws.findAllUsers();
		verify(dao,new Times(1)).getAllUsers();
	}
	
	@Test
	public void testFindUserNames() throws MalformedURLException{
		when(dao.successfulLoginUrl(anyString(), anyString())).thenReturn(new URL("http://localhost:8090/PiedPiperProject/index.html"));
		LoginDetails details=new LoginDetails();
		details.setUsername("name");
		details.setPassword("password");
		uws.findUserNamesPasswords(details);
		verify(dao,new Times(1)).successfulLoginUrl("name", "password");
	}
	
	@Test
	public void testFindUserByName(){
		when(dao.getUsersByName(anyString())).thenReturn(users);
		uws.findUserByName("name");
		verify(dao,new Times(1)).getUsersByName("name");
	}
	
	@Test
	public void testFindById(){
		when(dao.getUserById(anyInt())).thenReturn(user);
		uws.findUserById(22);
		verify(dao,new Times(1)).getUserById(22);
	}
	
	@Test
	public void testUpdateUser(){
		doNothing().when(dao).update(any(User.class));
		uws.updateUser(user);
		verify(dao, new Times(1)).update(user);
	}

}
