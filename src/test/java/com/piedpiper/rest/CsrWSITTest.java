package com.piedpiper.rest;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.CSRDao;
import com.piedpiper.dataimport.tabObjects.BaseData;

/**
 * @author James Runswick
 * @author Date Created 5 Jul 2016
 * @version 1.0
 *
 */
@RunWith(Arquillian.class)
public class CsrWSITTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		WebArchive testWar = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(CsrWS.class,CSRDao.class)
				.addPackage(BaseData.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		System.out.println(testWar);
		return testWar;
	}

	@EJB private CsrWS testDataIn;
	
	@Test
	@UsingDataSet({"EventCauseData.yml","basedata.yml"})
	public void testGetEventCauseForIMSISize() {
		List<Object[]> testList = testDataIn.getEventAndCauseId(344_930_000_000_011L);
		assertEquals(4,testList.size());
	}
	
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetCountForImsiSuccess() throws ParseException{
		long count = testDataIn.getCountForIMSI(344_930_000_000_011L,"2013-01-11 00:00","2013-01-12 00:00");
		assertEquals(4L,count);
	}
	@Test
	@UsingDataSet({"basedata.yml"})
	public void testGetCountForImsiFailure() throws ParseException{
		long count = testDataIn.getCountForIMSI(311_000_000_011L,"2013-01-11 00:00","2013-01-12 00:00");
		assertEquals(0L,count);
	}
	
	@Test
	@UsingDataSet({"basedata.yml","EventCauseData.yml"})
	public void testGetDistinctCauseCodeForIMSI() throws ParseException{
		List<Object[]> causeCodes= testDataIn.getCauseCode(344930000000011L);
		assertEquals(3,causeCodes.size());
		
		causeCodes= testDataIn.getCauseCode(300000000000000L);
		assertEquals(0,causeCodes.size());
	}
}
